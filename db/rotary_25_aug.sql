-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.19-0ubuntu0.14.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for db_rotaryfishtail_new
CREATE DATABASE IF NOT EXISTS `db_rotaryfishtail_new` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_rotaryfishtail_new`;


-- Dumping structure for table db_rotaryfishtail_new.tbl_articles
CREATE TABLE IF NOT EXISTS `tbl_articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned DEFAULT NULL,
  `order` tinyint(3) unsigned DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `status` tinyint(4) DEFAULT '0',
  `title` varchar(100) DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  `content` varchar(10000) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `image_title` varchar(100) DEFAULT NULL,
  `image_title_2` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `video_title` varchar(100) DEFAULT NULL,
  `video_url` varchar(200) DEFAULT NULL,
  `embed_code` varchar(10000) DEFAULT NULL,
  `author` int(10) DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `meta_key` varchar(100) DEFAULT NULL,
  `meta_desc` varchar(10000) DEFAULT NULL,
  `meta_robots` varchar(100) DEFAULT NULL,
  `url1` varchar(100) DEFAULT NULL,
  `url2` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `tbl_articles_ibfk_1` (`category_id`),
  KEY `tbl_articles_ibfk_3` (`author`),
  KEY `tbl_articles_ibfk_4` (`modified_by`),
  CONSTRAINT `FK_tbl_articles_tbl_categories` FOREIGN KEY (`category_id`) REFERENCES `tbl_categories` (`id`),
  CONSTRAINT `FK_tbl_articles_tbl_users` FOREIGN KEY (`author`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_articles_tbl_users_2` FOREIGN KEY (`modified_by`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_articles: ~21 rows (approximately)
/*!40000 ALTER TABLE `tbl_articles` DISABLE KEYS */;
INSERT INTO `tbl_articles` (`id`, `category_id`, `order`, `name`, `status`, `title`, `slug`, `content`, `image`, `image_title`, `image_title_2`, `video`, `video_title`, `video_url`, `embed_code`, `author`, `modified_by`, `meta_key`, `meta_desc`, `meta_robots`, `url1`, `url2`, `created_at`, `updated_at`) VALUES
	(51, 4, NULL, 'how it works', 1, NULL, 'how-it-works', '<ol><li><b><h2>step 1</h2></b><p>ok man</p></li><li><b><h2>step 2</h2></b><p>yes man</p></li><li><b><h2>step 2</h2></b><p>be ready</p></li></ol>', '524440_4362996394967_145472913_n.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:21:41', '0000-00-00 00:00:00'),
	(52, 38, NULL, 'How can i create Campaign here', 1, NULL, 'how-can-i-create-campaign-here', '<p>Just sign up and your campaign is ready</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:22:28', '2015-07-25 14:03:15'),
	(53, 4, NULL, 'about us', 1, NULL, 'about-us', '<p>Ourlibrary.com.au is fundraising website</p><p>This is dummy text for our library website. This is dummy text for our library website. This is dummy text for our library website.This is dummy text for our library website.This is dummy text for our library website. This is dummy text for our library website. This is dummy text for our library website.This is dummy text for our library website.This is dummy text for our library website.<br></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:22:43', '2015-07-25 13:32:46'),
	(54, 4, NULL, 'useful links', 1, NULL, 'useful-links', '<ul><li><a target="_blank" rel="nofollow" href="http://www.google.com">List items 1</a></li><li>List items 2</li><li>List items 3</li><li>list items 4</li></ul><br><br>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:23:12', '2015-07-25 17:59:03'),
	(57, 38, NULL, 'What is Ourlibrary.com.au', 1, NULL, 'what-is-ourlibrarycomau', 'Ourlibrary.com.au is fundraising website&nbsp;', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:22:28', '2015-07-25 14:03:12'),
	(60, 38, NULL, 'How i will receive raised donation once campaign get completed?', 1, NULL, 'how-i-will-receive-raised-donation-once-campaign-get-completed', '<p>You will recieve your raised donation through your PAYPAL or BANK ACCOUNT</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 20:31:28', NULL),
	(61, 38, NULL, 'Alexis Mercer', 1, NULL, 'alexis-mercer', '<p>dfdfdf</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 20:45:32', NULL),
	(62, 38, NULL, 'Devin Hartman', 1, NULL, 'devin-hartman', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto nihil saepe laborum architecto, consequuntur mollitia eius quas at cumque rem, nobis reiciendis doloribus. Blanditiis dolorem temporibus suscipit tempora nostrum saepe.</p><div><br></div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 21:45:07', '2015-07-25 18:01:10'),
	(63, 38, NULL, 'Kimberly Nunez', 1, NULL, 'kimberly-nunez', '<p>cc</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 19:20:47', NULL),
	(64, 38, NULL, 'Cain Ball', 1, NULL, 'cain-ball', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 19:22:13', NULL),
	(65, 38, NULL, 'Kadeem Rios', 3, NULL, 'kadeem-rios', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 19:22:39', NULL),
	(66, 38, NULL, 'Brenna Barron', 3, NULL, 'brenna-barron', '<p>dfdfd</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 19:24:02', NULL),
	(112, 39, NULL, 'bye', 3, 'm', 'm', '<p>ce</p>', '0', 'pe', 'we', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-23 21:25:15', NULL),
	(113, 39, NULL, 'Micath Cobb', 1, 'bytwo', 'micah-cobb', '<p>cc</p>', '0', 'pp', 'ww', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-23 23:20:52', NULL),
	(114, 39, NULL, 'Kyle Delgado', 3, 'bythree', 'kyle-delgado', '<p>cc</p>', '0', 'ppp', 'www', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-23 23:21:59', NULL),
	(115, 39, NULL, 't', 1, 'b', 't', '<p><i>?c</i><br></p>', 'rtn-rama.jpg', 'p', 'w', NULL, NULL, NULL, NULL, 166, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-24 00:26:10', NULL),
	(116, 39, NULL, 'Denise Dean', 3, 'Delectus ea proident maiores sed aut nisi illo ullam molestiae', 'denise-dean', '<p>c</p>', '', 'Consequatur non dolore numquam voluptatibus ducimus beatae numquam eiusmod eu impedit nobis rem', 'Quos accusamus cillum sed tempore natus ex consequatur quos sed exercitationem reprehenderit maxime ', NULL, NULL, NULL, NULL, 157, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-24 21:11:03', NULL),
	(117, 39, NULL, 'a', 3, 'Fugiat aut autem saepe enim repudiandae aliqua Neque nisi accusamus laborum inventore ipsam incididu', 'a', '<p>CDFDF</p>', '', 'Error odit aliqua Quidem consequuntur sed fuga Commodi quas', 'Sint nihil occaecat perspiciatis ex', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-24 21:20:24', NULL),
	(118, 39, NULL, 'b', 1, 'mm', 'mm', '<p>cc</p>', '0', 'pe', 'w', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-24 21:40:09', NULL),
	(119, 39, NULL, 'Velit alias facilis sequi proident et nisi incididunt dolore et nostrum qui qui ab cillum ut', 3, 'Eve Spencer', 'eve-spencer', '<p>fdfdfdeee</p>', '', 'Architecto deserunt qui natus rerum fugiat id nihil', 'Blanditiis tempore pariatur Numquam consectetur voluptas et ad Nam porro suscipit sunt alias iure ea', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 09:14:32', NULL),
	(121, 39, NULL, 'aa', 1, 'p', 'aa', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '', 'aaa', 'aae', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 06:44:53', NULL),
	(127, 40, NULL, 'p1', 1, 'aa', 'aa', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'favico_24.png', 'aaa', 'aa', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 06:44:53', NULL),
	(136, 40, NULL, 'we', 3, NULL, 'we', '<p>sd<br></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:12:38', NULL),
	(137, 40, NULL, 'tdfdsf', 3, NULL, 'tdfdsf', '<p>dsfsdfdsf<br></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:14:16', NULL),
	(140, 40, NULL, 'tt', 3, NULL, 'tt', '<p>ttt<br></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:28:55', NULL),
	(141, 40, NULL, 'heyge', 1, NULL, 'hey', '<p>tttee<br></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:29:19', NULL),
	(144, 41, NULL, 'none', 1, NULL, 'none', '<p>tttee<br></p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:29:19', NULL);
/*!40000 ALTER TABLE `tbl_articles` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_campaign
CREATE TABLE IF NOT EXISTS `tbl_campaign` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `user_id` int(22) NOT NULL,
  `fund_category_id` int(22) DEFAULT NULL,
  `campaign_title` varchar(111) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `target_amount` int(11) NOT NULL,
  `description` text NOT NULL,
  `pic` varchar(25000) NOT NULL,
  `video` varchar(111) NOT NULL,
  `document` varchar(111) DEFAULT NULL,
  `fb_link` varchar(111) NOT NULL,
  `twitter_link` varchar(111) NOT NULL,
  `enable_comment` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `starting_at` date NOT NULL,
  `ending_at` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `url_link` varchar(50) DEFAULT NULL,
  `success_story` varchar(20000) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `fund_category_id` (`fund_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_campaign: ~16 rows (approximately)
/*!40000 ALTER TABLE `tbl_campaign` DISABLE KEYS */;
INSERT INTO `tbl_campaign` (`id`, `user_id`, `fund_category_id`, `campaign_title`, `slug`, `target_amount`, `description`, `pic`, `video`, `document`, `fb_link`, `twitter_link`, `enable_comment`, `status`, `starting_at`, `ending_at`, `created_at`, `updated_at`, `url_link`, `success_story`, `approved_by`) VALUES
	(1, 156, 1, 'Nepal Earthquake', 'nepal-earthquake', 2000, '<p>As someone who has considered Nepal home for many years, the shock on hearing the news of the&nbsp;<a href="http://www.theguardian.com/world/live/2015/apr/27/nepal-earthquake-rescue-efforts-thousands-seek-shelter-rolling-report" target="" rel="">earthquake that has devastated the country</a>&nbsp;was extreme. I felt pained at being away from home – cut off in rural Cambodia – at a time like this, impotent and powerless.</p><p>The quake has left thousands dead, many more injured and even more without shelter. What we also know from tracking events in natural disasters all over the world is that the situation only gets worse in the weeks following the event, as hospitals become overwhelmed, basic supplies become scarce and those living in temporary shelters succumb to exposure and disease. The example fresh in everyone’s minds is the&nbsp;<a href="http://www.theguardian.com/world/ng-interactive/2015/jan/16/the-haiti-earthquake-five-years-on-then-and-now" target="" rel="">2010 earthquake in Haiti</a>&nbsp;– a country in a similar economic condition to Nepal and which, despite an enormous influx of international aid, is still recovering from the disaster five years later.</p>', 'a:2:{i:0;s:25:"1435444146_2cqupQi7FM.png";i:1;s:25:"1435444523_f3ivbRxQDF.png";}', '', NULL, '', '', 0, 4, '2015-06-28', '2015-07-28', '2015-06-28 00:25:07', '2015-06-29 18:40:29', '/', NULL, NULL),
	(2, 158, 1, 'Soluta quae magnam rerum ipsum fugiat amet unde', 'soluta-quae-magnam-rerum-ipsum-fugiat-amet-unde', 63, 'Adipisci molestias Nam proident, debitis fugit, sunt, est, quas eos sint unde labore saepe aute anim mollit praesentium odio.', '', '', NULL, '', '', 0, 4, '1980-07-12', '1976-01-20', '2015-06-29 05:39:14', '0000-00-00 00:00:00', NULL, NULL, NULL),
	(3, 159, 1, 'Fuga Nulla commodi irure labore velit quo sit cumque aperiam et totam aut nisi reprehenderit', 'fuga-nulla-commodi-irure-labore-velit-quo-sit-cumque-aperiam-et-totam-aut-nisi-reprehenderit', 48, 'A amet, vero aut laboriosam, exercitation praesentium consequuntur vel repudiandae est libero ad dolor sunt.', 'a:2:{i:0;s:25:"1435594272_8BDVJJAfXn.jpg";i:1;s:25:"1435677954_tM3HAc69zw.jpg";}', '', NULL, '', '', 0, 1, '2015-06-29', '1994-12-08', '2015-06-29 05:44:35', '2015-06-29 18:29:24', '/ramesh-gurung', NULL, NULL),
	(4, 159, 1, 'Aliqua Voluptate tempor et possimus animi Nam in', 'aliqua-voluptate-tempor-et-possimus-animi-nam-in', 61, '<p>dfdfd</p>', '', '', NULL, '', '', 0, 5, '2015-06-29', '1970-01-01', '2015-06-29 17:58:48', '2015-06-29 17:59:02', '/', NULL, NULL),
	(5, 159, 1, 'Magni veritatis vero delectus explicabo Quae molestiae culpa in rerum iure', 'magni-veritatis-vero-delectus-explicabo-quae-molestiae-culpa-in-rerum-iure', 30, '<p>dd</p>', '', '', NULL, '', '', 0, 0, '2015-06-29', '1970-01-01', '2015-06-29 18:30:26', '0000-00-00 00:00:00', '/', NULL, NULL),
	(6, 156, 1, 'Incididunt qui quaerat in sed corporis consequatur repudiandae non at voluptatem dolore nisi', 'incididunt-qui-quaerat-in-sed-corporis-consequatur-repudiandae-non-at-voluptatem-dolore-nisi', 14, 'ddf', '', '', NULL, '', '', 0, 4, '1970-01-01', '1970-01-01', '2015-06-29 18:40:47', '2015-06-30 17:20:19', '/', NULL, NULL),
	(7, 160, 1, 'Praesentium est officia sed est officia et velit aut quas minim aspernatur possimus placeat magnam perferendis', 'praesentium-est-officia-sed-est-officia-et-velit-aut-quas-minim-aspernatur-possimus-placeat-magnam-p', 49, 'Minim labore quae mollitia dolor autem quia et id suscipit aut at.', '', 'Consequatur eos ut perspiciatis veritatis minus enim eu iure sit quis', NULL, '', '', 0, 1, '1975-12-24', '1992-12-26', '2015-07-01 05:27:18', '0000-00-00 00:00:00', NULL, NULL, NULL),
	(8, 161, 1, 'Omnis reprehenderit blanditiis corrupti dolores rerum rerum', 'omnis-reprehenderit-blanditiis-corrupti-dolores-rerum-rerum', 13, 'Dolor illo et reiciendis eos, suscipit consequatur fugiat veritatis corrupti.', '', 'Laborum Tenetur qui magna sunt nostrum sed enim recusandae Duis enim', NULL, '', '', 0, 1, '2015-12-22', '1985-12-21', '2015-07-01 05:28:25', '0000-00-00 00:00:00', NULL, NULL, NULL),
	(9, 162, 1, 'Sit proident eligendi at ut architecto veniam qui ut nulla dolor qui', 'sit-proident-eligendi-at-ut-architecto-veniam-qui-ut-nulla-dolor-qui', 86, 'Laborum commodo voluptates temporibus tempor sint rerum veniam, quos quia voluptatem vel laboris ea ipsum odio veniam, sunt.', '', '', NULL, '', '', 0, 1, '2003-05-07', '2011-01-07', '2015-07-01 18:52:06', '0000-00-00 00:00:00', NULL, NULL, NULL),
	(10, 163, 1, '1212', 'dolore-deleniti-voluptatem-natus-incididunt-ex-reprehenderit-qui-ut', 90, 'Rerum dolorem aut quis laborum. Deleniti nulla rerum dolorum at placeat, sapiente at maxime.', 'a:1:{i:0;s:25:"1436979440_n5hwXYG7qq.png";}', '', NULL, '', '', 0, 1, '2002-02-16', '1993-01-23', '2015-07-01 19:05:26', '2015-07-15 18:57:38', NULL, NULL, NULL),
	(11, 164, 1, 'Dolor alias eaque est ex adipisci voluptatem Eos quisquam quod voluptate voluptatem atque aut inventore quis qu', 'dolor-alias-eaque-est-ex-adipisci-voluptatem-eos-quisquam-quod-voluptate-voluptatem-atque-aut-invent', 63, 'Fugiat est, porro dolores beatae cupiditate modi error ea labore.', '', '', NULL, '', '', 0, 1, '2003-06-01', '2013-09-11', '2015-07-15 18:48:56', '0000-00-00 00:00:00', NULL, NULL, NULL),
	(12, 165, 1, '12', 'fuga-consequatur-qui-adipisicing-modi-quisquam-quia-perferendis', 6, 'Dolor cupidatat velit do in doloribus similique duis dolore ut rerum fugiat, nisi.', '', '', NULL, '', '', 0, 1, '2006-08-03', '1972-08-20', '2015-07-15 18:50:37', '0000-00-00 00:00:00', NULL, NULL, NULL),
	(13, 166, 1, '15Ducimus sed voluptatum dolorem saepe ea et incidunt', '15ducimus-sed-voluptatum-dolorem-saepe-ea-et-incidunt', 82, 'Inventore velit, veritatis perspiciatis, dolorem aspernatur et et consectetur earum animi, voluptatem. Commodi ipsam eum aliquip voluptate cillum dolor aut.', 'a:2:{i:0;s:49:"10174914_734281233393801_137121333087423423_n.jpg";i:1;s:36:"524440_4362996394967_145472913_n.jpg";}', 'Dolorem qui cupidatat voluptatum consequatur et velit do dolor quos quasi tempora adipisicing distinctio Reicie', NULL, '', '', 0, 1, '2015-07-15', '2015-07-31', '2015-07-15 19:00:38', '0000-00-00 00:00:00', NULL, NULL, NULL),
	(14, 156, 2, 'Cupidatat laudantium velit commodi ipsam dolor fugiat laborum Sunt dolor autem minima a fugit consequat Cumque ', 'cupidatat-laudantium-velit-commodi-ipsam-dolor-fugiat-laborum-sunt-dolor-autem-minima-a-fugit-conseq', 20, '<p>ee</p>', 'a:1:{i:0;s:25:"1438273697_Yi3fiW0v8J.jpg";}', '', NULL, '', '', 0, 0, '2015-07-16', '2015-08-30', '2015-07-16 05:24:56', '0000-00-00 00:00:00', '/', NULL, NULL),
	(21, 176, 2, 'Nam fugiat molestiae quasi soluta et ducimus enim ipsa tempora necessitatibus dolor est voluptate illum porro q', 'nam-fugiat-molestiae-quasi-soluta-et-ducimus-enim-ipsa-tempora-necessitatibus-dolor-est-voluptate-il', 79, 'Neque laboriosam, nihil ipsum non sed duis architecto molestias rem dolores cillum culpa est nostrud quo.', '', 'Fugiat mollitia sed tenetur enim quis id eaque qui nemo', NULL, '', '', 0, 1, '2015-07-30', '2015-08-30', '2015-07-30 19:10:22', '0000-00-00 00:00:00', NULL, NULL, NULL),
	(22, 177, 1, 'ftejrljek', 'ftejrljek', 1000, 'f', '', '', NULL, '', '', 0, 1, '2015-07-30', '2015-08-30', '2015-07-30 19:12:42', '0000-00-00 00:00:00', NULL, NULL, NULL);
/*!40000 ALTER TABLE `tbl_campaign` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_categories
CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(25) NOT NULL,
  `content` varchar(2000) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `image_title` varchar(50) DEFAULT NULL,
  `url` varchar(100) NOT NULL,
  `order` int(10) unsigned DEFAULT NULL,
  `published` tinyint(3) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author` int(10) DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`name`),
  KEY `tbl_categories_ibfk_1` (`parent_id`),
  KEY `tbl_categories_ibfk_2` (`author`),
  KEY `tbl_categories_ibfk_3` (`modified_by`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_categories: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_categories` DISABLE KEYS */;
INSERT INTO `tbl_categories` (`id`, `parent_id`, `name`, `slug`, `content`, `image`, `image_title`, `url`, `order`, `published`, `created_at`, `author`, `modified_by`, `updated_at`, `status`) VALUES
	(4, NULL, 'uncategoried', 'uncategoried', 'uncategoried', '', '', '', 1, 1, '2015-03-01 12:17:53', 0, NULL, '2015-07-25 11:52:26', 1),
	(38, NULL, 'Help', 'help', 'help', '', '', '', 1, 1, '2015-03-01 12:17:53', 0, NULL, '2015-07-25 11:52:26', 1),
	(39, NULL, 'messages', 'messages', 'help', '', '', '', 1, 1, '2015-03-01 12:17:53', 0, NULL, '2015-07-25 11:52:26', 1),
	(40, NULL, 'projects', 'projects', 'projects', '', '', '', 1, 1, '2015-03-01 12:17:53', 0, NULL, '2015-07-25 11:52:26', 1),
	(41, NULL, 'news', 'news', 'news', '', '', '', 1, 1, '2015-03-01 12:17:53', 0, NULL, '2015-07-25 11:52:26', 1);
/*!40000 ALTER TABLE `tbl_categories` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_donar
CREATE TABLE IF NOT EXISTS `tbl_donar` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_donar: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_donar` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_donar` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_donation
CREATE TABLE IF NOT EXISTS `tbl_donation` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `donar_id` int(22) NOT NULL,
  `campaign_id` int(22) NOT NULL,
  `amount` int(22) NOT NULL,
  `comment` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `donar_id` (`donar_id`),
  KEY `campaign_id` (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_donation: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_donation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_donation` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_fund_categories
CREATE TABLE IF NOT EXISTS `tbl_fund_categories` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `name` varchar(111) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `glyphicon` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_fund_categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_fund_categories` DISABLE KEYS */;
INSERT INTO `tbl_fund_categories` (`id`, `name`, `slug`, `description`, `image`, `glyphicon`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'fund_category', 'fund_category', 'Provident, nesciunt, a cupiditate praesentium ut aut maxime reprehenderit, quia culpa quis repudiandae eiusmod quia pariatur? Nisi.', '', 'fa fa-heart', 1, '2015-06-27 18:06:03', '2015-07-01 04:57:41'),
	(2, 'Jaime Gibson', 'jaime-gibson', 'Sint nisi minima sit elit, sed tenetur irure duis aliquid velit quis saepe aliquam et enim aut nesciunt, deleniti.', '', 'Voluptas eius commodi facere dolor', 1, '2015-07-15 19:26:57', NULL);
/*!40000 ALTER TABLE `tbl_fund_categories` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_groups
CREATE TABLE IF NOT EXISTS `tbl_groups` (
  `id` int(22) unsigned NOT NULL AUTO_INCREMENT,
  `parent_group_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `tbl_groups_ibfk_1` (`parent_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_groups: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_groups` DISABLE KEYS */;
INSERT INTO `tbl_groups` (`id`, `parent_group_id`, `name`, `slug`, `desc`, `status`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Superadmin', 'superadmin', 'superadmin desc', 1, '2015-01-28 00:36:41', NULL),
	(2, 1, 'Admin', 'admin', '', 1, '2015-01-28 00:37:01', NULL),
	(3, NULL, 'Donee', 'donee', 'Quibusdam at sint voluptas eum debitis accusamus voluptatem voluptatem, non eu et in et illum, ab et sed ut.', 1, '2015-03-28 13:22:30', NULL),
	(4, NULL, 'Facebook User', 'facebook_user', 'd\r\n', 1, '2015-05-19 17:00:54', NULL);
/*!40000 ALTER TABLE `tbl_groups` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_group_permissions
CREATE TABLE IF NOT EXISTS `tbl_group_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned DEFAULT NULL,
  `permission_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_group_permissions_ibfk_1` (`group_id`),
  KEY `tbl_group_permissions_ibfk_2` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3780 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_group_permissions: ~79 rows (approximately)
/*!40000 ALTER TABLE `tbl_group_permissions` DISABLE KEYS */;
INSERT INTO `tbl_group_permissions` (`id`, `group_id`, `permission_id`, `created_at`, `updated_at`) VALUES
	(1749, 8, 80, '2015-06-08 15:07:15', NULL),
	(1750, 8, 82, '2015-06-08 15:07:15', NULL),
	(1751, 8, 83, '2015-06-08 15:07:15', NULL),
	(1752, 8, 84, '2015-06-08 15:07:15', NULL),
	(1753, 8, 85, '2015-06-08 15:07:15', NULL),
	(1754, 8, 86, '2015-06-08 15:07:15', NULL),
	(3665, 2, 5, '2015-06-27 21:42:35', NULL),
	(3666, 2, 6, '2015-06-27 21:42:35', NULL),
	(3667, 2, 7, '2015-06-27 21:42:35', NULL),
	(3668, 2, 96, '2015-06-27 21:42:35', NULL),
	(3669, 2, 97, '2015-06-27 21:42:35', NULL),
	(3670, 2, 98, '2015-06-27 21:42:35', NULL),
	(3671, 2, 99, '2015-06-27 21:42:35', NULL),
	(3672, 2, 55, '2015-06-27 21:42:35', NULL),
	(3673, 2, 61, '2015-06-27 21:42:35', NULL),
	(3674, 2, 64, '2015-06-27 21:42:35', NULL),
	(3675, 2, 65, '2015-06-27 21:42:35', NULL),
	(3676, 2, 66, '2015-06-27 21:42:36', NULL),
	(3677, 2, 67, '2015-06-27 21:42:36', NULL),
	(3678, 2, 68, '2015-06-27 21:42:36', NULL),
	(3679, 2, 70, '2015-06-27 21:42:36', NULL),
	(3680, 2, 72, '2015-06-27 21:42:36', NULL),
	(3681, 2, 73, '2015-06-27 21:42:36', NULL),
	(3682, 2, 74, '2015-06-27 21:42:36', NULL),
	(3683, 2, 76, '2015-06-27 21:42:36', NULL),
	(3684, 2, 77, '2015-06-27 21:42:36', NULL),
	(3685, 2, 78, '2015-06-27 21:42:36', NULL),
	(3686, 2, 79, '2015-06-27 21:42:36', NULL),
	(3687, 2, 80, '2015-06-27 21:42:36', NULL),
	(3688, 2, 82, '2015-06-27 21:42:36', NULL),
	(3689, 2, 83, '2015-06-27 21:42:36', NULL),
	(3690, 2, 84, '2015-06-27 21:42:36', NULL),
	(3691, 2, 85, '2015-06-27 21:42:36', NULL),
	(3692, 2, 86, '2015-06-27 21:42:36', NULL),
	(3693, 2, 100, '2015-06-27 21:42:36', NULL),
	(3694, 2, 91, '2015-06-27 21:42:36', NULL),
	(3737, 1, 5, '2015-07-31 22:40:16', NULL),
	(3738, 1, 6, '2015-07-31 22:40:16', NULL),
	(3739, 1, 7, '2015-07-31 22:40:16', NULL),
	(3740, 1, 96, '2015-07-31 22:40:16', NULL),
	(3741, 1, 97, '2015-07-31 22:40:16', NULL),
	(3742, 1, 98, '2015-07-31 22:40:16', NULL),
	(3743, 1, 99, '2015-07-31 22:40:16', NULL),
	(3744, 1, 8, '2015-07-31 22:40:17', NULL),
	(3745, 1, 9, '2015-07-31 22:40:17', NULL),
	(3746, 1, 10, '2015-07-31 22:40:17', NULL),
	(3747, 1, 17, '2015-07-31 22:40:17', NULL),
	(3748, 1, 15, '2015-07-31 22:40:17', NULL),
	(3749, 1, 16, '2015-07-31 22:40:17', NULL),
	(3750, 1, 87, '2015-07-31 22:40:17', NULL),
	(3751, 1, 88, '2015-07-31 22:40:17', NULL),
	(3752, 1, 89, '2015-07-31 22:40:17', NULL),
	(3753, 1, 90, '2015-07-31 22:40:17', NULL),
	(3754, 1, 92, '2015-07-31 22:40:17', NULL),
	(3755, 1, 93, '2015-07-31 22:40:17', NULL),
	(3756, 1, 55, '2015-07-31 22:40:17', NULL),
	(3757, 1, 61, '2015-07-31 22:40:17', NULL),
	(3758, 1, 64, '2015-07-31 22:40:17', NULL),
	(3759, 1, 65, '2015-07-31 22:40:17', NULL),
	(3760, 1, 66, '2015-07-31 22:40:17', NULL),
	(3761, 1, 67, '2015-07-31 22:40:17', NULL),
	(3762, 1, 68, '2015-07-31 22:40:17', NULL),
	(3763, 1, 70, '2015-07-31 22:40:17', NULL),
	(3764, 1, 72, '2015-07-31 22:40:17', NULL),
	(3765, 1, 73, '2015-07-31 22:40:18', NULL),
	(3766, 1, 74, '2015-07-31 22:40:18', NULL),
	(3767, 1, 76, '2015-07-31 22:40:18', NULL),
	(3768, 1, 77, '2015-07-31 22:40:18', NULL),
	(3769, 1, 78, '2015-07-31 22:40:18', NULL),
	(3770, 1, 79, '2015-07-31 22:40:18', NULL),
	(3771, 1, 101, '2015-07-31 22:40:18', NULL),
	(3772, 1, 80, '2015-07-31 22:40:18', NULL),
	(3773, 1, 82, '2015-07-31 22:40:18', NULL),
	(3774, 1, 83, '2015-07-31 22:40:18', NULL),
	(3775, 1, 84, '2015-07-31 22:40:18', NULL),
	(3776, 1, 85, '2015-07-31 22:40:18', NULL),
	(3777, 1, 86, '2015-07-31 22:40:18', NULL),
	(3778, 1, 100, '2015-07-31 22:40:18', NULL),
	(3779, 1, 91, '2015-07-31 22:40:18', NULL);
/*!40000 ALTER TABLE `tbl_group_permissions` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_logs
CREATE TABLE IF NOT EXISTS `tbl_logs` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `user_id` int(22) NOT NULL,
  `log_type` varchar(111) NOT NULL,
  `log_description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_logs` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_permissions
CREATE TABLE IF NOT EXISTS `tbl_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_permission_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fk_parent_permission_id` (`parent_permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_permissions: ~62 rows (approximately)
/*!40000 ALTER TABLE `tbl_permissions` DISABLE KEYS */;
INSERT INTO `tbl_permissions` (`id`, `parent_permission_id`, `name`, `slug`, `desc`, `created_at`) VALUES
	(5, NULL, 'Administrator User', 'administrator-user', '', '2015-01-29 00:56:32'),
	(6, 5, 'add user', 'add-user', '', '2015-01-29 00:58:23'),
	(7, 5, 'edit user', 'edit-user', '', '2015-01-29 00:58:38'),
	(8, NULL, 'Administrator  Permission', 'administrator-permission', '', '2015-01-29 00:59:51'),
	(9, 8, 'add Permission', 'add-permission', '', '2015-01-29 01:00:02'),
	(10, 8, 'edit Permission', 'edit-permission', '', '2015-01-29 01:00:11'),
	(15, NULL, 'Administrator Group', 'administrator-group', '', '2015-02-01 15:40:08'),
	(16, 15, 'Add Group', 'add-group', '', '2015-02-01 15:45:09'),
	(17, 8, 'delete Permission', 'delete-permission', '', '2015-02-20 22:21:45'),
	(31, 30, 'list category', 'list-category', '', '2015-02-23 01:40:17'),
	(32, 30, 'add category', 'add-category', '', '2015-02-23 01:43:00'),
	(33, 30, 'edit category', 'edit-category', '', '2015-02-23 01:43:17'),
	(34, 30, 'delete category', 'delete-category', '', '2015-02-23 01:43:27'),
	(35, 30, 'activate category', 'activate-category', '', '2015-02-23 01:43:35'),
	(36, 30, 'block category', 'block-category', '', '2015-02-23 01:43:43'),
	(38, 37, 'list menu', 'list-menu', '', '2015-02-23 01:40:17'),
	(39, 37, 'add menu', 'add-menu', '', '2015-02-23 01:43:00'),
	(40, 37, 'edit menu', 'edit-menu', '', '2015-02-23 01:43:17'),
	(41, 37, 'delete menu', 'delete-menu', '', '2015-02-23 01:43:27'),
	(42, 37, 'activate menu', 'activate-menu', '', '2015-02-23 01:43:35'),
	(43, 37, 'block menu', 'block-menu', '', '2015-02-23 01:43:43'),
	(48, 47, 'add article', 'add-article', '', '2015-02-23 01:43:00'),
	(49, 47, 'edit article', 'edit-article', '', '2015-02-23 01:43:17'),
	(50, 47, 'delete article', 'delete-article', '', '2015-02-23 01:43:27'),
	(51, 47, 'activate article', 'activate-article', '', '2015-02-23 01:43:35'),
	(52, 47, 'block article', 'block-article', '', '2015-02-23 01:43:43'),
	(53, 47, 'list article', 'list-article', '', '2015-02-23 01:40:17'),
	(54, 47, 'view article', 'view-article', '', '2015-02-23 01:40:17'),
	(55, NULL, 'administrator setting', 'administrator-setting', '', '2015-01-29 00:56:32'),
	(61, NULL, 'administrator fund category', 'administrator-fund-category', '', '2015-05-24 21:40:30'),
	(64, 61, 'add-fund-category', 'add-fund-category', '', '2015-05-24 21:40:30'),
	(65, 61, 'edit-fund-category', 'edit-fund-category', '', '2015-05-24 21:40:30'),
	(66, 61, 'delete-fund-category', 'delete-fund-category', '', '2015-05-24 21:40:30'),
	(67, 61, 'activate-fund-category', 'activate-fund-category', '', '2015-05-24 21:40:30'),
	(68, 61, 'block-fund-category', 'block-fund-category', '', '2015-05-24 21:40:30'),
	(70, 61, 'list-fund-category', 'list-fund-category', '', '2015-05-24 21:40:30'),
	(72, NULL, 'administrator-campaign', 'administrator-campaign', '', '2015-05-24 21:40:30'),
	(73, 72, 'add-campaign', 'add-campaign', '', '2015-05-24 21:40:30'),
	(74, 72, 'edit-campaign', 'edit-campaign', '', '2015-05-24 21:40:30'),
	(76, 72, 'delete-campaign', 'delete-campaign', '', '2015-05-24 21:40:30'),
	(77, 72, 'activate-campaign', 'activate-campaign', '', '2015-05-24 21:40:30'),
	(78, 72, 'block-campaign', 'block-campaign', '', '2015-05-24 21:40:30'),
	(79, 72, 'list-campaign', 'list-campaign', '', '2015-05-24 21:40:30'),
	(80, NULL, 'administrator-donee', 'administrator-donee', '', '2015-06-07 21:43:27'),
	(82, 80, 'list-donee', 'list-donee', '', '2015-06-07 21:44:53'),
	(83, 80, 'add-donee', 'add-donee', '', '2015-06-07 21:45:20'),
	(84, 80, 'activate-donee', 'activate-donee', '', '2015-06-07 21:45:43'),
	(85, 80, 'block-donee', 'block-donee', '', '2015-06-07 21:45:56'),
	(86, 80, 'delete-donee', 'delete-donee', '', '2015-06-07 21:46:07'),
	(87, 15, 'edit group', 'edit-group', '', '2015-06-08 08:51:37'),
	(88, 15, 'activate-group', 'activate-group', '', '2015-06-08 14:52:22'),
	(89, 15, 'block-group', 'block-group', '', '2015-06-08 14:52:37'),
	(90, 15, 'delete-group', 'delete-group', '', '2015-06-08 14:52:54'),
	(91, NULL, 'administrator-donation', 'administrator-donation', '', '2015-06-07 21:43:27'),
	(92, 15, 'update-permission-group', 'update-permission-group', '', '2015-06-19 09:44:57'),
	(93, 15, 'List Group', 'list-group', '', '2015-06-19 09:46:11'),
	(96, 5, 'activate user', 'activate-user', '', '2015-01-29 00:58:38'),
	(97, 5, 'block user', 'block-user', '', '2015-01-29 00:58:38'),
	(98, 5, 'delete user', 'delete-user', '', '2015-01-29 00:58:38'),
	(99, 5, 'list user', 'list-user', '', '2015-01-29 00:58:38'),
	(100, 80, 'edit-donee', 'edit-donee', '', '2015-06-20 18:40:28'),
	(101, 72, 'success-campaign', 'success-campaign', 'success-campaign desc', '2015-07-31 22:38:31');
/*!40000 ALTER TABLE `tbl_permissions` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_settings
CREATE TABLE IF NOT EXISTS `tbl_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `value` longtext NOT NULL,
  `autoload` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_settings: ~49 rows (approximately)
/*!40000 ALTER TABLE `tbl_settings` DISABLE KEYS */;
INSERT INTO `tbl_settings` (`id`, `name`, `slug`, `value`, `autoload`) VALUES
	(1, 'email', 'email', 'info@ourlibraray.com', 1),
	(2, 'cell', 'cell', '9806677215', 0),
	(3, 'address', 'address', 'Melbourne , Austrailia', 0),
	(5, 'facebook_link', 'facebook_link', 'https://www.facebook.com/ourlibrary.com', 0),
	(6, 'twitter_link', 'twitter_link', 'https://www.twitter.com/ourlibrary.com', 0),
	(7, 'google_plus_link', 'google_plus_link', 'https://www.googleplus.com/ourlibrary.com', 0),
	(8, 'Site Name', 'site_name', 'Rotary Club Of Pokhara Fishtail', 0),
	(9, 'Site Url', 'site_url', 'Rotary Club Of Pokhara Fishtail', 0),
	(12, 'club_name', 'club_name', 'Rotary Club Of Pokhara Fishtail', 1),
	(13, 'club_number', 'club_number', 'Club No. 63638', 1),
	(15, 'slogan_head', 'slogan_head', 'Service above Self', 1),
	(16, 'slogan_body', 'slogan_body', 'ROTARY INTERNATIONAL', 1),
	(17, 'slogan_dist', 'slogan_dist', 'Dist. 32921', 1),
	(18, 'other_site_1_name', 'other_site_1_name', 'Fishtail Fund', 1),
	(19, 'other_site_1_url', 'other_site_1_url', 'http://www.fishtailfund.com/', 1),
	(20, 'other_site_2_name', 'other_site_2_name', 'Annapurna Fund', 1),
	(21, 'other_site_2_url', 'other_site_2_url', 'http://www.fishtailfund.np/e', 1),
	(22, 'slider_show_total', 'slider_show_total', '5', 1),
	(23, 'sidebar1_content', 'sidebar1_content', 'Info\n[We meet on every Friday]\nat Atithi Resort & Spa\nLakeside, Pokhara, Nepal.\nFeb-Oct. (6:00pm to 7:00pm) \nNov-Jan. (5:30pm to 6:30pm).', 1),
	(24, 'sidebar2_content', 'sidebar2_content', 'NOTICE\n[We meet on every Friday]\nat Atithi Resort & Spa\nLakeside, Pokhara, Nepal.\nFeb-Oct. (6:00pm to 7:00pm) \nNov-Jan. (5:30pm to 6:30pm).', 1),
	(25, 'footer_message', 'footer_message', ' 2013-2015 | Rotary Club of Fishtail Pokhara. All Rights Reserved. ', 1),
	(26, 'slider_category_id', 'slider_category_id', '4', 1),
	(27, 'telephone_1', 'telephone_1', '+977 98560-21476', 1),
	(28, 'telephone_2', 'telephone_2', 't2', 1),
	(29, 'mobile_1', 'mobile_1', 'm1', 1),
	(30, 'mobile_2', 'mobile_2', 'm2', 1),
	(31, 'email_1', 'email_1', 'info@rotaryfishtail.org', 1),
	(32, 'email_2', 'email_2', 'e2', 1),
	(33, 'location', 'location', 'Rotary Club of Pokhara Fishtail, Atithi Resort & Spa, Lakeside, Pokhara, Nepal', 1),
	(34, 'contact', 'contact', 'Submit', 1),
	(41, 'header_logo', 'header_logo', 'add-bank-receipt.png', 1),
	(42, 'sidebar1_picture', 'sidebar1_picture', 'user1.png', 1),
	(43, 'sidebar2_picture', 'sidebar2_picture', 'list-report.png', 1),
	(44, 'no_of_message_on_home_page', 'no_of_message_on_home_page', '1', 1),
	(46, 'no_of_projects_on_home_page', 'no_of_projects_on_home_page', '2', 1),
	(49, 'message_category_id', 'message_category_id', '39', 1),
	(61, 'project_category_id', 'project_category_id', '40', 1),
	(62, 'no_of_news_on_home_page', 'no_of_news_on_home_page', '31', 1),
	(67, 'news_category_id', 'news_category_id', '41', 1);
/*!40000 ALTER TABLE `tbl_settings` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_users
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `facebook_id` bigint(20) DEFAULT NULL,
  `username` varchar(111) NOT NULL,
  `group_id` int(22) unsigned NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `pass` varchar(111) NOT NULL,
  `address_unit` varchar(111) NOT NULL,
  `address_street` varchar(111) NOT NULL,
  `address_suburb` varchar(111) NOT NULL,
  `address_state_id` int(11) NOT NULL,
  `contact_emails` varchar(111) NOT NULL,
  `status` int(11) NOT NULL,
  `verification_code` varchar(111) NOT NULL,
  `last_login_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `facebook_id` (`facebook_id`,`username`),
  KEY `FK_tbl_users_tbl_groups` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_users: ~16 rows (approximately)
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` (`id`, `facebook_id`, `username`, `group_id`, `first_name`, `last_name`, `email`, `pass`, `address_unit`, `address_street`, `address_suburb`, `address_state_id`, `contact_emails`, `status`, `verification_code`, `last_login_date`, `created_at`, `updated_at`) VALUES
	(25, NULL, 'superadmin', 1, 'ram', 'bahadur', 'raxizel@gmai.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'pokhara', 'kajipokhri', 'suburb', 0, '', 1, '', '2015-05-09', '2015-05-09 00:00:00', '2015-05-09 00:00:00'),
	(156, NULL, 'ramesh', 3, 'ag', 'g', 'raxizel@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '', '', 0, '', 1, '', '0000-00-00', '2015-06-27 18:04:37', '2015-07-15 18:21:57'),
	(157, NULL, 'basant', 2, '', '', 'bitterbasan@gmail.com', '56bb0e63248924a02231595d7c9a3619b0c95764', '', '', '', 0, '', 1, '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(158, NULL, 'fikakav', 3, 'Cassady', 'Beasley', 'pokana@gmail.com', 'ac748cb38ff28d1ea98458b16695739d7e90f22d', 'Consequuntur reprehenderit error sint repellendus', 'Voluptate inventore elit cupidatat iusto reprehenderit in voluptas aperiam consequatur', 'Enim non molestiae dolorum veritatis voluptas similique reprehenderit dolorem', 0, '', 0, 'b50f3242a2676c48696ece83f11b5209', '0000-00-00', '2015-06-29 05:39:06', '0000-00-00 00:00:00'),
	(159, NULL, 'donald', 3, 'Yuli', 'Hines', 'jiqeliweta@yahoo.com', 'ac748cb38ff28d1ea98458b16695739d7e90f22d', 'Velit sed excepturi officiis impedit magna itaque consequatur libero distinctio Veniam anim', 'Lorem qui nulla laboriosam sunt enim', 'Blanditiis iusto enim enim perferendis sed culpa quidem unde', 0, '', 1, '4c6903100519b99c05cf313837abef8c', '0000-00-00', '2015-06-29 05:44:30', '0000-00-00 00:00:00'),
	(160, NULL, 'qecaqo', 3, 'donee', 'Church', 'fivup@gmail.com', 'ac748cb38ff28d1ea98458b16695739d7e90f22d', 'Ut voluptates amet dolor eiusmod nemo eos aut tempor error laudantium tempore quia', 'Quia ipsum molestiae commodi nostrud illum sit est dolorum quos veniam voluptas non', 'Enim dolorem a anim corporis consequatur Officia distinctio Iure necessitatibus fugiat at et ea ut quod corpori', 0, '', 0, 'c00495319b84e5d54d260ab296b74409', '0000-00-00', '2015-07-01 05:23:28', '0000-00-00 00:00:00'),
	(161, NULL, 'javulef', 3, 'Duncan', 'Whitney', 'gobyh@hotmail.com', 'ac748cb38ff28d1ea98458b16695739d7e90f22d', 'Accusamus doloribus dolores in et non est inventore', 'Esse voluptate esse debitis quia', 'Consequatur non sit non doloribus lorem enim numquam consequatur sunt', 0, '', 0, '4db68db9d37ac318860634a8fe971d3c', '0000-00-00', '2015-07-01 05:28:21', '0000-00-00 00:00:00'),
	(162, NULL, 'hyhusag', 3, 'Raven', 'Ford', 'raxizel+1@gmail.com', 'ac748cb38ff28d1ea98458b16695739d7e90f22d', 'Harum sed molestiae sint id magna excepteur repellendus Molestiae nostrud placeat ea eos magni voluptatem excep', 'Ut quasi quisquam eiusmod dolore', 'Ab perspiciatis a maxime ex sit', 0, '', 0, 'af1c8de9aaee5bbb480d86b263f50c41', '0000-00-00', '2015-07-01 18:51:57', '0000-00-00 00:00:00'),
	(163, NULL, 'jyfomubaw', 3, 'Reece', 'Holcomb', 'raxizel+2@gmail.com', 'ac748cb38ff28d1ea98458b16695739d7e90f22d', 'Sint aperiam omnis commodo aut quam adipisci enim adipisicing sit nisi non consequuntur dolor aut odio', 'Provident in pariatur Qui aperiam molestiae est quibusdam excepturi minim', 'Nisi natus est est officiis cupidatat laboris expedita beatae soluta eos amet iure nostrud et consectetur nobis', 0, '', 0, 'bc47a36f6a84251d0efc596d599fb2ec', '0000-00-00', '2015-07-01 19:05:22', '0000-00-00 00:00:00'),
	(164, NULL, 'hufebu', 3, 'Grant', 'Woods', 'goqu@yahoo.com', 'ac748cb38ff28d1ea98458b16695739d7e90f22d', '', '', '', 0, '', 0, '2fca5d7a3488e511eafd4ad89e0792ad', '0000-00-00', '2015-07-15 18:48:53', '0000-00-00 00:00:00'),
	(165, NULL, 'mydehadufu', 3, 'Slade', 'Knowles', 'raxizel+1@gmail.com', 'ac748cb38ff28d1ea98458b16695739d7e90f22d', '', '', '', 0, '', 0, '4343e5eea79ae2948a14df0f528f766e', '0000-00-00', '2015-07-15 18:50:34', '0000-00-00 00:00:00'),
	(166, NULL, 'wurola', 3, 'Chaim', 'Kemp', 'femufu@hotmail.com', 'ac748cb38ff28d1ea98458b16695739d7e90f22d', '', '', '', 0, '', 3, 'f5df710f05d873bd1fc82bb851ece3e1', '0000-00-00', '2015-07-15 19:00:15', '0000-00-00 00:00:00'),
	(177, 966641686703115, 'Grg_Rax', 4, 'Grg', 'Rax', 'raxizel@gmail.com', '', 'u', 's', 'ss', 0, '', 0, 'b9aaac368045da5460c6094ffcca1aff', '0000-00-00', '2015-07-30 19:11:50', '0000-00-00 00:00:00'),
	(178, NULL, 'hey', 1, '', '', 'man', '', '', '', '', 0, '', 3, '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(179, NULL, 'hey', 1, '', '', 'email.com', '', '', '', '', 0, '', 0, '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(180, NULL, 'u', 1, '', '', 'e@gmail.com', '', '', '', '', 0, '', 0, '', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
