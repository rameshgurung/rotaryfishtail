-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for db_rotaryfishtail_new
CREATE DATABASE IF NOT EXISTS `db_rotaryfishtail_new` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_rotaryfishtail_new`;


-- Dumping structure for table db_rotaryfishtail_new.tbl_articles
CREATE TABLE IF NOT EXISTS `tbl_articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  `content` varchar(10000) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `order` tinyint(3) unsigned DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `image_title` varchar(100) DEFAULT NULL,
  `image_title_2` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `video_title` varchar(100) DEFAULT NULL,
  `video_url` varchar(200) DEFAULT NULL,
  `embed_code` varchar(10000) DEFAULT NULL,
  `author` int(10) DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `meta_key` varchar(100) DEFAULT NULL,
  `meta_desc` varchar(10000) DEFAULT NULL,
  `meta_robots` varchar(100) DEFAULT NULL,
  `url1` varchar(100) DEFAULT NULL,
  `url2` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `tbl_articles_ibfk_1` (`category_id`),
  KEY `tbl_articles_ibfk_3` (`author`),
  KEY `tbl_articles_ibfk_4` (`modified_by`),
  CONSTRAINT `FK_tbl_articles_tbl_categories` FOREIGN KEY (`category_id`) REFERENCES `tbl_categories` (`id`),
  CONSTRAINT `FK_tbl_articles_tbl_users` FOREIGN KEY (`author`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_articles_tbl_users_2` FOREIGN KEY (`modified_by`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_articles: ~72 rows (approximately)
/*!40000 ALTER TABLE `tbl_articles` DISABLE KEYS */;
INSERT INTO `tbl_articles` (`id`, `category_id`, `name`, `title`, `slug`, `content`, `status`, `order`, `image`, `image_title`, `image_title_2`, `video`, `video_title`, `video_url`, `embed_code`, `author`, `modified_by`, `meta_key`, `meta_desc`, `meta_robots`, `url1`, `url2`, `created_at`, `updated_at`) VALUES
	(51, 4, 'ABOUT ROTARY CLUB', NULL, 'how-it-works', '<div>\r\n<p>Rotary International is a voluntary organization of business and professional leaders which provide humanitarian servicesone to help to build goodwill and peace in the world. There are more than 1.2 million (12 Lakhs) members 34000 Rotary clubs in over 200 countries and geographical areas. [Rotary\'s top priority is the global graphical eradication of polio.] Founded in Chicago in 1905, The Rotary foundation has awarded more than U.S. $ 2.1 billion in grants, which are administered at the local level by Rotary Clubs.</p>\r\n</div>\r\n<div>\r\n<h2>Focus Areas</h2>\r\n<br />\r\n<h3>POLIO ERADICATION :</h3>\r\nSince 1985 Polio plus program was created to Immunize all the world\'s children against Polio. To date Rotary has contributed U.S. $ 800 million Protection of more than two billion children in 122 countries. Rotary has successfully acquired an additional U.S. $200 million toward a U.S. $ 355 million challenge grant won the Bill Melinda Gates Foundation.<br /><br />\r\n<h3>INTERNATIONAL EDUCATION :</h3>\r\nRotary is the world\'s largest Privately funded source of international Scholarships and each year about 1000 university students receive Rotary Scholarships to study abroad. Rotary also co-ordinate a high School student exchange program that has sent some 8000 students abroad for three months to a year. Rotary world peace fellows are up to 110 scholars in 2003, sponsored each year to study at one of the 8 universities for Inter studies in peace and conflict resolutions.<br /><br />\r\n<h3>HUMANITARIAN PROJECTS :</h3>\r\nThus, Rotary Clubs initiate thousand of humanitarian service projects every year, The Rotary Foundation grants 3H (Health Hunger humanity) grants, District designates fund (DDF) etc</div>\r\n<p><br /><br /></p>\r\n<div>\r\n<h2>The four way Test</h2>\r\nThe four way test was created by Rotarian Hebert &amp; now followed by Rotarian world wide in there business and professional lives. This test includes the 4 questions about the things we say or do, which creates a life of high values and moral character.<br /><br />\r\n<h4>The four way test are</h4>\r\nIs it the truth ? <br />Is it fair to all concerned ? <br />Will it build goodwill &amp; better friendship? <br />Will it be beneficial to all concerned?</div>', 1, NULL, '524440_4362996394967_145472913_n.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:21:41', '2015-09-03 17:34:10'),
	(52, 38, 'How can i create Campaign here', NULL, 'how-can-i-create-campaign-here', '<p>Just sign up and your campaign is ready</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:22:28', '2015-07-25 14:03:15'),
	(53, 4, 'about us', NULL, 'about-us', '<p>Rotary International is a voluntary organization of business and professional leaders which provide humanitarian servicesone to help to build goodwill and peace in the world. There are more than 1.2 million (12 Lakhs) members 34000 Rotary clubs in over 200 countries and geographical areas. [Rotary\'s top priority is the global graphical eradication of polio.] Founded in Chicago in 1905, The Rotary foundation has awarded more than U.S. $ 2.1 billion in grants, which are administered at the local level by Rotary Clubs.</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:22:43', '2015-09-01 19:21:24'),
	(54, 4, 'MISSION OF ROTARY', NULL, 'useful-links', '<p>The mission of Rotary International is to support its member clubs in fulfilling the Object of Rotary by:</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>- Fostering unity among member clubs</li>\r\n<li>- Strengthening and expanding Rotary around the world</li>\r\n<li>- Communicating worldwide the work of Rotary</li>\r\n<li>- Providing a system of international administration</li>\r\n</ul>\r\n<p><br /><br /><br /></p>\r\n<h1>OBJECTIVES OF ROTARY</h1>\r\n<p>The object of Rotary is to encourage and foster the ideal of service as a basis of worthy enterprise and in particular, to encourage and foster:</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li><strong>First :</strong> The development of acquaintance as an opportunity for service.</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li><strong>Second:</strong> High ethical standard in business and professions: the recognition of the worthiness of all useful occupation: the dignifying of each Rotarian\'s occupation as an opportunity:</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li><strong>Third: </strong>The application on the ideal of service in each Rotarian\'s personal, business, and community life.</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li><strong>Fourth:</strong> The advancement of international understanding goodwill and peace through a world fellowship of ideal of service</li>\r\n</ul>\r\n<p>&nbsp;</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:23:12', '2015-09-03 17:40:28'),
	(57, 38, 'What is Ourlibrary.com.au', NULL, 'what-is-ourlibrarycomau', 'Ourlibrary.com.au is fundraising website&nbsp;', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 14:22:28', '2015-07-25 14:03:12'),
	(60, 38, 'How i will receive raised donation once campaign get completed?', NULL, 'how-i-will-receive-raised-donation-once-campaign-get-completed', '<p>You will recieve your raised donation through your PAYPAL or BANK ACCOUNT</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 20:31:28', NULL),
	(61, 38, 'Alexis Mercer', NULL, 'alexis-mercer', '<p>dfdfdf</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-25 20:45:32', NULL),
	(62, 38, 'Devin Hartman', NULL, 'devin-hartman', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto nihil saepe laborum architecto, consequuntur mollitia eius quas at cumque rem, nobis reiciendis doloribus. Blanditiis dolorem temporibus suscipit tempora nostrum saepe.</p><div><br></div>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-07-25 21:45:07', '2015-07-25 18:01:10'),
	(63, 38, 'Kimberly Nunez', NULL, 'kimberly-nunez', '<p>cc</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 19:20:47', NULL),
	(64, 38, 'Cain Ball', NULL, 'cain-ball', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 19:22:13', NULL),
	(65, 38, 'Kadeem Rios', NULL, 'kadeem-rios', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 19:22:39', NULL),
	(66, 38, 'Brenna Barron', NULL, 'brenna-barron', '<p>dfdfd</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-07-26 19:24:02', NULL),
	(112, 39, 'bye', 'm', 'm', '<p>ce</p>', 3, NULL, '0', 'pe', 'we', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-23 21:25:15', NULL),
	(113, 39, 'Micath Cobb', 'bytwo', 'micah-cobb', '<p>cc</p>', 3, NULL, '0', 'pp', 'ww', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-23 23:20:52', NULL),
	(114, 39, 'Kyle Delgado', 'bythree', 'kyle-delgado', '<p>cc</p>', 3, NULL, '0', 'ppp', 'www', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-23 23:21:59', NULL),
	(115, 39, 't', 'b', 't', '<p><i>?c</i><br></p>', 1, NULL, 'rtn-rama.jpg', 'p', 'w', NULL, NULL, NULL, NULL, 166, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-24 00:26:10', NULL),
	(116, 39, 'Denise Dean', 'Delectus ea proident maiores sed aut nisi illo ullam molestiae', 'denise-dean', '<p>c</p>', 3, NULL, '', 'Consequatur non dolore numquam voluptatibus ducimus beatae numquam eiusmod eu impedit nobis rem', 'Quos accusamus cillum sed tempore natus ex consequatur quos sed exercitationem reprehenderit maxime ', NULL, NULL, NULL, NULL, 157, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-24 21:11:03', NULL),
	(117, 39, 'a', 'Fugiat aut autem saepe enim repudiandae aliqua Neque nisi accusamus laborum inventore ipsam incididu', 'a', '<p>CDFDF</p>', 3, NULL, '', 'Error odit aliqua Quidem consequuntur sed fuga Commodi quas', 'Sint nihil occaecat perspiciatis ex', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-24 21:20:24', NULL),
	(118, 39, 'b', 'mm', 'mm', '<p>cc</p>', 3, NULL, '0', 'pe', 'w', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-24 21:40:09', NULL),
	(119, 39, 'Velit alias facilis sequi proident et nisi incididunt dolore et nostrum qui qui ab cillum ut', 'Eve Spencer', 'eve-spencer', '<p>fdfdfdeee</p>', 3, NULL, '', 'Architecto deserunt qui natus rerum fugiat id nihil', 'Blanditiis tempore pariatur Numquam consectetur voluptas et ad Nam porro suscipit sunt alias iure ea', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 09:14:32', NULL),
	(127, 40, 'p1', 'aa', 'aa', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 3, NULL, 'favico_24.png', 'aaa', 'aa', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 06:44:53', NULL),
	(136, 40, 'we', NULL, 'we', '<p>sd<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:12:38', NULL),
	(137, 40, 'tdfdsf', NULL, 'tdfdsf', '<p>dsfsdfdsf<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:14:16', NULL),
	(140, 40, 'tt', NULL, 'tt', '<p>ttt<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:28:55', NULL),
	(141, 40, 'heyge', NULL, 'hey', '<p>tttee<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:29:19', NULL),
	(144, 41, 'none', NULL, 'none', '<p>tttee<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 10:29:19', NULL),
	(145, 39, 'ne', 'b', 'ne', '<p>a?</p>', 3, NULL, '', 'p', 'w', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 18:07:28', NULL),
	(146, 40, 'Blair', NULL, 'blair-torres', '<p>dfdfdfee</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 18:07:59', NULL),
	(147, 40, 'Rama Foster', NULL, 'rama-foster', '<p>cceeE</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-08-25 19:35:07', NULL),
	(148, 40, 'oe', NULL, 'pone', '<p>c</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 20:09:23', NULL),
	(149, 40, 'ptwoe', NULL, 'ptwo', '<p>cedfdfdfdf</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-25 20:09:36', NULL),
	(151, 39, 'Rama Fostere', 'b', 'rama-fostere', '<p>c<br></p>', 3, NULL, '9781405304504L_001.jpg', 'o', 'wess', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 08:30:17', NULL),
	(152, 41, 'tn', NULL, 'tn', '<p>cc<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 09:06:56', NULL),
	(153, 41, 'fdsfj', NULL, 'fdsfj', '<p>dfdfdf<br></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 09:09:21', NULL),
	(154, 41, 'nc', NULL, 'nc', '<p><i>ccc</i><br></p><br><br>', 3, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 09:15:05', NULL),
	(155, 41, 'DLSJFLKJ', NULL, 'dlsjflkj', '<p>DJFKDF<br></p>', 3, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 09:17:50', NULL),
	(156, 41, 'dfdsjf', NULL, 'dfdsjf', '<p>dljfldk<br></p>', 3, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 09:19:43', NULL),
	(157, 41, 'tfdfdf', NULL, 'tfdfdf', '<p>t<br></p>', 3, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 09:22:00', NULL),
	(158, 41, 'adfjdskeh', NULL, 'dfjdsk', 'a<p>djfkde<br></p>', 3, NULL, '6a00e553bd675c88340148c6693d12970c.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-08-26 19:03:18', NULL),
	(159, 39, 'fjdsfdhfdsf', 'dfdf', 'fjdsfdhfdsf', '<p>dlfjdsk<br></p>', 3, NULL, '0', 'p', 'w', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 19:08:55', NULL),
	(160, 39, 'dfdf', 'fjdsfdhfdsfdfd', 'fjdsfdhfdsfdfd', '<p>dlfjdsk<br></p>', 3, NULL, 'lion-riding_1739860i.jpg', 'p', 'w', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 19:09:54', NULL),
	(161, 39, 'GARY C.K. HUANG', 'MESSAGE FROM ROTARY INTERNATIONAL PRESIDENT', 'message-from-rotary-international-president', '<h5>Dear President Rtn. Rama Kanta Baral</h5><p>Congratulations on your assuming the office of President of your club for the year 2014-15. I also congratulate your dedicated team of club officers for a very successful year ahead</p><p><b>Light up Rotary</b>&nbsp;is the annual theme give by Rotary International President Gary C.K. Huang. who has challenged us to take action and make Rotary strong. One of the many ways to&nbsp;<b>Light Up Rotary</b>is to plan and host Rotary Day as an event to make new friends, exchange ideas, and take action to improve their local community and the world. Let us do it earnestly by highlighting the work of ordinary Rotary members doing extraordinary humanitarian work. Let us take this challenge together and Light Up Rotary.</p><p>All we asay and do must satisfy our own conscience as a Rotary Leade whether it benefits our clubs and our communities by adding some value is someone\'s life, which would ultimately satisfy our instinct of doing something good in the world. By satisfying our instinct, we can get joy at work and start enjoying Rotary.</p><p>Enjoy Rotary, by all means.</p><p>Yours-In-Rotary?</p>', 1, NULL, 'rtn-gary.jpg', 'RI President', '2014-15', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 19:48:16', NULL),
	(162, 39, 'RABINDRA PIYA', 'MESSAGE FROM DISTRICT GOVERNER', 'message-from-district-governer', '<h5>Dear President Rtn. Rama Kanta Baral</h5><p>Congratulations on your assuming the office of President of your club for the year 2014-15. I also congratulate your dedicated team of club officers for a very successful year ahead</p><p><b>Light up Rotary</b>&nbsp;is the annual theme give by Rotary International President Gary C.K. Huang. who has challenged us to take action and make Rotary strong. One of the many ways to&nbsp;<b>Light Up Rotary</b>is to plan and host Rotary Day as an event to make new friends, exchange ideas, and take action to improve their local community and the world. Let us do it earnestly by highlighting the work of ordinary Rotary members doing extraordinary humanitarian work. Let us take this challenge together and Light Up Rotary.</p><p>All we asay and do must satisfy our own conscience as a Rotary Leade whether it benefits our clubs and our communities by adding some value is someone\'s life, which would ultimately satisfy our instinct of doing something good in the world. By satisfying our instinct, we can get joy at work and start enjoying Rotary.</p><p>Enjoy Rotary, by all means.</p><p>Yours-In-Rotary?</p>', 1, NULL, 'rtn-rabindra.jpg', 'District Governor', '2013-14', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 19:49:38', NULL),
	(163, 39, 'RAMA KANTA BARAL', 'MESSAGE FROM PRESIDENT', 'message-from-president', '<h5>Dear Fellow Rotarians </h5><p>While assuming the office of the President of our prestigious club for 2014/15, at the very out set, I would like to extend my deep sense of gratitude to all my fellow rotarians for giving me this opportunity to act whole heartedly in giving direction and drive to the plans, aims and objectives of the club.</p><p>Working hand in hand with members of the club associated with different professions and business sectors, I feel somewhat elated to think, though tough, I will be working effectively to forward the humanatarian service in the true spirit of our commitment "Service above self". I feel pride to recount here a host of noble activities successfully conducted by this club since its inception in many areas of human concern such as education, health, poverty reduction, peace and family values and so on.</p><p>In the changing times all of us need to make more and more valuable contribution in the priority areas of our service for the society. Side by side, I feel, we should try to explore new vistas of service sectors as far as practicable.</p><p>In maintaining and developing international relation, activating highly reliable partnership with different sister clubs and in many service areas of noble human interest, our club has been sucessful to produce a host of landmarks. It is a matter of pride for us all. In these and other areas, I will be endevouring my level best to maintain and enhance the glorious strides of this club. No doubt, much has been done for the benefit of Nepalese people from the future vision program of the Rotary Foundation and it is heartening to mention-all of us, rotarians are moving ahead in genuine team spirit. Even so, we need to go on structuring the slots of time to make the range of our mission higher and wider.</p><p>Before giving a close to my words, I would also like to draw the attention of all, specifically the rotarians to keep in mind the superbly remarkable theme, \'LIGHT UP ROTARY\', forwarded by the President of the Rotary International for 2014/15, Mr, Gary C.K. Huang. Introducing the theme, \'LIGHT UP ROTARY\', respected Gary has given a new concept, \'Rotary Days\' which, I believe, will assist us more in our existing membership drive, strengthening our club\'s relationships with local institutions and community members as well as in improving the image of our club not only in local and national level but in international level as well.</p><p>Finally, I would like to invite all the fellow rotarians and people in general to come forward with open mind to share their noble ideas and spirit to widen the horizon of the service of this club for creating better situations of life in this part of the world and of course, keeping our minds and eyes open for the well being of the people in the international level too.</p><p>With bountiful thanks to you all,?</p>', 1, NULL, 'rtn-rama.jpg', 'District Governor', '2014 - 15', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 19:50:36', NULL),
	(164, 40, 'Club\'s Future Projects', NULL, 'clubs-future-projects', '<ul><li>1. Fishtail Fund (A long-term project running in 5th year)</li><li>2. Children of Annapurna Fund (A long-term project running in 3rd year)</li><li>3.Micro Loan Scheme(A long-term project initiated in2012)</li><li>4. Water Supply to Schools and Community at Majhthum, Bhadaure Tamagi, Kaski</li><li>5. Water Filtration project for schools, health institutes and community?</li></ul>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:02:43', NULL),
	(165, 40, 'Club\'s Long-term Projects', NULL, 'clubs-long-term-projects', '<p>Phasellus mattis tincidunt nibh.?<br></p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:03:39', NULL),
	(166, 40, 'Club\'s Past Projects', NULL, 'clubs-past-projects', '<ul><li>1. Water Supply project at Sahara Academy</li><li>2. Water Supply to Majhkot Village and Sivalaya Secondary School of Thumki</li><li>3. Water Supply to Dhruba Higher Secondary School, Tarkang, Kaski?</li></ul>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:04:45', NULL),
	(167, 41, 'MAJTHUM DRINKING WATER PROJECT HANDOVER', NULL, 'majthum-drinking-water-project-handover', '<p>Rotary Club of Pokhara Fishtail has decided to handover " Majthum Drinking Water Project" to the community on 6th December 2014.?<br></p>', 1, NULL, 'majthum1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:06:32', NULL),
	(168, 39, 'Wynne Castro', 'Non commodo sunt officia illo maiores cupidatat aut iure ut exercitationem ad et sit', 'wynne-castro', '<p>fdfdf</p>', 3, NULL, '', 'Sed qui doloremque molestiae ut incididunt repellendus Voluptate ab quo et et rerum magni cupidatat ', 'Rerum hic et ut eu eos eos', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:07:12', NULL),
	(169, 39, 'Hanna Tanner', 'Possimus enim id eu vel qui harum sit corporis lorem voluptas ullamco quasi atque quidem est reprehe', 'hanna-tanner', '<p>fdfdf</p>', 3, NULL, '', 'Incididunt ut et aut maxime pariatur Labore officiis modi est tempor fugit aliquip cupiditate et ass', 'In consequat Enim ipsam do adipisicing tempora', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:07:49', NULL),
	(170, 39, 'Rajah Jackson', 'Eos est laborum sunt aute magnam quos', 'rajah-jackson', '<p>fdsjfds</p>', 3, NULL, '', 'Laborum Nulla in inventore eum omnis enim esse eu corporis ut veniam error maxime qui unde temporibu', 'Voluptatum accusantium eos consectetur excepturi provident autem debitis explicabo Ut quas ab doloru', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:08:14', NULL),
	(171, 40, 'Giacomo Mcclain', NULL, 'giacomo-mcclain', '<p>fdfdsf</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 20:08:37', NULL),
	(172, 4, 'jk', NULL, 'jk', '<p>jlk</p>', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 23:38:47', NULL),
	(173, 40, 'Rhonda English', NULL, 'rhonda-english', '<p>kjlkj</p>', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 23:40:13', NULL),
	(174, 41, 'dfdlsj', NULL, 'dfdlsj', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 23:42:59', NULL),
	(175, 41, 'dell', NULL, 'dell', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 23:44:42', NULL),
	(176, 39, 'Drake Doylefdfd', NULL, 'drake-doylefdfd', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 23:45:19', NULL),
	(177, 4, 'Leo Hunt', NULL, 'leo-hunt', '', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-26 23:46:38', NULL),
	(178, 41, '12121', NULL, '12121', '<p>12121</p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-28 19:42:49', NULL),
	(179, 41, 'isset', NULL, 'isset', '<p class="para rdfs-comment">Determine if a variable is set and is not <strong><code>NULL</code></strong>.</p>\r\n<p class="para">If a variable has been unset with <span class="function"><a class="function" href="http://php.net/manual/en/function.unset.php">unset()</a></span>, it will no longer be set. <span class="function"><strong>isset()</strong></span> will return <strong><code>FALSE</code></strong> if testing a variable that has been set to <strong><code>NULL</code></strong>. Also note that a null character (<em>"\\0"</em>) is not equivalent to the PHP <strong><code>NULL</code></strong> constant.</p>\r\n<p class="para">If multiple parameters are supplied then <span class="function"><strong>isset()</strong></span> will return <strong><code>TRUE</code></strong> only if all of the parameters are set. Evaluation goes from left to right and stops as soon as an unset variable is encountered.</p>', 3, NULL, '3-tips-ui-router-600x250.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-28 19:48:51', NULL),
	(180, 40, 'Brynn Spencer', NULL, 'brynn-spencer', '<p>df</p>', 3, NULL, NULL, 'Quam ipsa sint eius dolorem est', NULL, NULL, 'Quasi cum deserunt reprehenderit cum odio mollitia ut dolore illo sequi eu sit labore quia magni ani', NULL, 'Ea incidunt, ab aliquam eveniet, possimus, doloribus rerum explicabo. Consequatur.', 25, NULL, 'Commodi exercitation eveniet molestias quis id qui incidunt sunt id itaque id velit', 'Dolor lorem commodo blanditiis autem praesentium reprehenderit libero in aute', 'Aperiam et assumenda consectetur mollitia sint quod sed', NULL, NULL, '2015-08-28 20:03:42', NULL),
	(181, 41, 'n', NULL, 'n', '<p>c</p>', 3, NULL, NULL, 'it', NULL, NULL, 'vt', NULL, 'embed', 25, NULL, 'mk', 'md', 'mr', 'http://url', NULL, '2015-08-28 20:09:03', NULL),
	(182, 41, 'Nero Careye', NULL, 'nero-carey', '<p>cce</p>', 3, NULL, '232828-1406991727.jpg', 'iIllum consequat Dolores fuga Eligendi provident deserunt velit enim exercitation sunt suscipit vero', NULL, NULL, 'vFuga Voluptas incidunt itaque omnis rerum molestiae nobis velit', 'http://v', 'eNostrud voluptatem. Eaque vel delectus, cum nemo non tempore, consequat. Reprehenderit quo maiores ut assumenda accusantium.', 25, 25, 'kMaiores quo et repellendus Aut velit hic est amet a', 'dAut ab veniam necessitatibus voluptate', 'rLaborum placeat alias aut aut', 'http://uEsse et amet veniam minima non excepturi numquam ex non et ex sit qui irure delectus aut dig', NULL, '2015-08-28 20:09:45', '2015-08-28 17:55:57'),
	(183, 44, 'Kai Nolan', NULL, 'kai-nolan', '<p>test</p>', 1, NULL, NULL, 'Delectus sit reprehenderit cupidatat deserunt provident dolore dolorem dolore ex dolores doloribus n', NULL, NULL, 'Qui neque rerum aut autem est', 'http://Quae provident reprehenderit aut aut consequat Assumenda quia', 'Quam quia nobis dolores quia neque ea est, labore et suscipit quae laboriosam, tempora in reprehenderit itaque dolorem id.', 25, NULL, 'Et nulla obcaecati sed aperiam officia id tenetur velit et dolor totam exercitation nihil', 'Fugit anim voluptatem quam ducimus magnam duis dolore aliqua Do dolores Nam sit dolore delectus ad ut', 'Tempore ut dolores doloribus provident elit in aut enim pariatur Nihil ut non tenetur', 'http://Eum voluptatem sint doloremque ut ex ullamco est', NULL, '2015-08-29 16:02:45', NULL),
	(184, 41, 'Board of Directors 2014/15', NULL, 'board-of-directors-201415', '<table width="995">\r\n<tbody>\r\n<tr>\r\n<td><img src="../uploads/tinyMCE/main/member/Acr27052689167828-22888.jpg" alt="" width="262" height="327" /></td>\r\n<td><img  100px; margin-left: 100px;" src="../uploads/tinyMCE/main/member/Acr2705268916782819877.jpg" alt="" width="274" height="338" /></td>\r\n</tr>\r\n</tbody>\r\n</table>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-08-31 11:37:06', '2015-08-31 07:53:50'),
	(185, 41, 'test article', NULL, 'test-article', '<p><img src="../uploads/tinyMCE/main/Chrysanthemum.jpg" alt="" width="137" height="158" /></p>', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-08-31 17:58:25', '2015-08-31 14:25:53'),
	(186, 38, 'Davis Jarvis', NULL, 'davis-jarvis', '<table  100px;" width="957">\r\n<tbody>\r\n<tr>\r\n<td>1</td>\r\n<td>2</td>\r\n</tr>\r\n</tbody>\r\n</table>', 1, NULL, NULL, 'Aut quam nesciunt in minima molestiae autem maiores', NULL, NULL, 'Et iste minim dolor voluptatem aut cum laborum Repellendus Sed illum architecto ullamco', 'http://Duis voluptates fuga Sed aute', 'Officia voluptatum doloremque assumenda illum, impedit, ratione eum dolore optio, et consequatur soluta cupidatat quia quia non excepturi itaque.', 25, 25, 'Voluptas est exercitationem ipsa occaecat voluptatum sint tempore ipsum eos ipsa non', 'Animi laboris nulla autem nisi quisquam laudantium officia distinctio Anim Nam sed quis exercitationem voluptatibus duis', 'Aute qui voluptas vel rerum dolore temporibus ullamco dolores magnam aut officiis iusto id consectet', 'http://Consectetur sunt accusantium sit blanditiis Nam similique consequatur ut officiis nobis commo', NULL, '2015-08-31 21:31:56', '2015-08-31 19:41:21'),
	(187, 39, 'Steven Levine', 'Unde nulla earum totam omnis ut saepe earum ratione quidem non', 'steven-levine', '<p>dfd</p>', 3, NULL, '', 'Eum ratione adipisci tempora quis voluptatem Corporis quia dolor quam mollit nisi cupidatat nostrum ', 'Provident blanditiis cillum non animi commodi quam neque laborum exercitation in hic cumque id conse', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-09-01 09:30:58', NULL),
	(188, 4, 'Andrew Hull', NULL, 'andrew-hull', '<p>dfdf</p>', 3, NULL, NULL, 'Eu voluptatem et vel omnis molestiae deleniti quam aute modi non in odio sed voluptas deserunt non', NULL, NULL, 'Nostrum voluptatem sit quo aut cumque cupiditate rerum quo ratione expedita consequatur excepturi om', 'http://Recusandae Rerum natus ad assumenda ut irure molestiae ut omnis mollitia sunt ipsam atque consequuntur magni tenetur deserunt reprehenderit velit', 'Distinctio. Esse, eiusmod omnis consectetur, dolorem aliquid debitis proident, est, possimus, Nam maiores ipsum enim quia et.', 25, NULL, 'Vero enim omnis dolores pariatur Quis porro iure laborum Ad magnam', 'Obcaecati molestiae porro et ipsam dignissimos expedita id doloribus qui soluta ea fuga Totam minima impedit', 'Voluptatibus amet cumque voluptatem Velit commodo saepe molestiae Nam molestiae dolores do nesciunt ', 'http://Voluptas veniam mollit suscipit fugiat voluptas voluptate aut nihil voluptatem officia sint n', NULL, '2015-09-01 09:31:33', NULL),
	(189, 4, 'test page', NULL, 'joel-collier', '<h1>fomat</h1>\r\n<p>font size</p>\r\n<p>text color</p>\r\n<p><img src="/cel/2015/aug/projects/rotaryfishtail/new/templates/assets/editor/tinymce/plugins/emoticons/img/smiley-cool.gif" alt="cool" /></p>\r\n<p>underline</p>\r\n<p>is a landlocked country situated in south Asia.</p>\r\n<p>Pokhara is a beautiful tourism city of Nepal.</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>1</li>\r\n<li>2</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>1</li>\r\n<li>2</li>\r\n</ul>\r\n<p>increase indent It is situated at the middle west part of Nepal. It is a city surrounded by beautiful hill and mountains full of Natural beauty with Phewa Lake, Seti River and the Annapurna Range. About 4 hundred thousand inhabit this beautiful peace of paradise.</p>\r\n<p><a title="facebook title" href="/cel/2015/aug/projects/rotaryfishtail/new/article/edit/facebook.com" target="_blank">facebook.com</a></p>\r\n<p>&nbsp;</p>\r\n<p>&hearts;</p>\r\n<hr />\r\n<p><a id="anchor"></a>&lt;!-- pagebreak --&gt;</p>\r\n<p>21:45:08</p>\r\n<p>2015-09-03</p>\r\n<p>9:45:16 PM</p>\r\n<p>09/&nbsp;&nbsp;03/2015</p>\r\n<p>&nbsp;</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>1</td>\r\n<td>2</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>rb</td>\r\n<td>1</td>\r\n<td colspan="2">2-3</td>\r\n</tr>\r\n<tr>\r\n<td>1</td>\r\n<td>2</td>\r\n<td>3</td>\r\n<td>4</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><br />The Rotary Club of Pokhara Fishtail was established in 2003 under the presidency of Rtn. Dr. Sunu Dulal as the chartered President. It is the 3rd Club of Pokhara City. It was Sponsored by Rotary club of Pokhara with the initiation of GSR Rtn. Sundar Kumar Shrestha, past president of the club . The club was chartered on 22nd October 2003 under the RI District 3290 (until 30 Jun 2008). There were 46 members at the time of charter.</p>\r\n<h2>President of the club</h2>\r\n<p>&nbsp;</p>\r\n<p>Rtn. Arjun Thapa PHF 2004/05<br />Rtn. L.B. Baniya PHF 2006/07<br />Rtn Binod Koirala PHF 2008/09<br />Rtn. Er. Suresh Pd. Shrestha PHF 2010/11<br />Rtn Raju Paudel PHF 2012/13<br />Rtn Ramakanta Baral 2014/15</p>\r\n<h2>Members of the Club as April 2012 with MPHF and PHF</h2>\r\n<p>&nbsp;</p>\r\n<p>1) PP Rtn. Dr. Sunu Dulal MPHF 6024267<br />2) PP Rtn. Arjun Thapa PHF 6025207<br />3) PP Rtn Bhuwansing Gurung PHF 6025176<br />4) PP Rtn. Binod Koirala PHF 6024268<br />5) PP Rtn. Jayendra Gauchan PHF 6025182<br />6) IPP. Rtn. Er.Suresh Pd. Shrestha PHF 6025192<br />7) President Rtn. Bishnu Hari Timilsina MPHF 6025173<br />8) Rtn. Tej Bahadur Gurung 6025213<br />9) Rtn. Maheswar Pahari 6025212<br />10) Rtn. Baburam Pathak 6025158<br />11) Rtn. Baikuntha Raj Timilsina 6025205<br />12) Rtn. Ishwori Prasad Sharma 6025180<br />13) Rtn. Om Prakash Gurung 6025223<br />14) Rtn. Deepak Sherchan PHF 6025209<br />15) Rtn. Takman Pradhananga 6025196<br />16) Rtn. Raju Paudel President Elect 6266997<br />17) Rtn. Naryan Pd. Sharma President Nominee PHF 6266996<br />18) Rtm. Ramakanta Barala PHF 6445793<br />19) Rtm. Er. Bishnu Raj Koirala PHF 7021199<br />20) Rtn. Jagan Subba Gurung PHF 6713387<br />21) Rtn. Rekha Karki PHF 6793193<br />22) Rtn. Narendra Pd limbu PHF 8052481<br />23) Rtn. Shesh bahadur Thapa 6621165<br />24) Rtn. Roshani Koirala 6445768<br />25) Rtn. Dr. Anil Babu Parajuli 8002485<br />26) Rtn. Ashok Chalise 6894606<br />27) Rtn. Nar bahadur Gurung 6445792<br />28) Rtn. Prof Dr. Keshav Datta Awasthi 6532203<br />29) Rtn. Shiva Prasad Parajuli 6793192<br />30) Rtn. Om Raj Paudel 7021194<br />31) Rtn. Dr. Suman Lal Shrestha 8052479<br />32) Rtn. Douglas Maclagan 8184876<br />33) Rtn. Ramesh Koirala 8245347<br />34) Rtn. Rabindra K.C. 8272169<br />35) Rtn. Damodar Basaula 8245351<br />36) Rtn. Bal Bahadur Thapa 8245350<br />37) Rtn. Dr. Niraj Kumar Dubey 8270934<br />38) Rtn. Laxmi Thapa Giri 8326502<br />39) Rtn. Dr. Prem Thapa 8200230<br />40) Rtn. Chandra Badadur Subedi 8426236</p>\r\n<p>&nbsp;</p>', 1, NULL, '11954655_1091123954231378_4880705577924777332_n.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-09-02 21:27:57', '2015-09-03 18:10:07'),
	(191, 41, 'test news 1', NULL, 'test-news-1', '<p>hjh</p>', 1, NULL, 'images.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-09-03 08:39:47', NULL),
	(192, 4, 'club profile', NULL, 'club-profile', '<div>\r\n<p>Nepal is a landlocked country situated in south Asia. Its area is 147000 of sq. km. and Population about 29 million. Pokhara is a beautiful tourism city of Nepal. It is situated at the middle west part of Nepal. It is a city surrounded by beautiful hill and mountains full of Natural beauty with Phewa Lake, Seti River and the Annapurna Range. About 4 hundred thousand inhabit this beautiful peace of paradise.</p>\r\n<br />\r\n<p>The Rotary Club of Pokhara Fishtail was established in 2003 under the presidency of Rtn. Dr. Sunu Dulal as the chartered President. It is the 3rd Club of Pokhara City. It was Sponsored by Rotary club of Pokhara with the initiation of GSR Rtn. Sundar Kumar Shrestha, past president of the club . The club was chartered on 22nd October 2003 under the RI District 3290 (until 30 Jun 2008). There were 46 members at the time of charter.</p>\r\n</div>\r\n<div>\r\n<h2>President of the club</h2>\r\n<br />\r\n<ul>\r\n<li>Rtn. Dr. Sunu Dulal - MPHF 2003/04</li>\r\n<li>Rtn. Arjun Thapa PHF 2004/05</li>\r\n<li>Rtn. Capt.Bhuwansing Gurung PHF 2005/06</li>\r\n<li>Rtn. L.B. Baniya PHF 2006/07</li>\r\n<li>Rtn. Dhruba K.C 2007/08</li>\r\n<li>Rtn Binod Koirala PHF 2008/09</li>\r\n<li>Rtn. Jayandra Gauchan PHF 2009/10</li>\r\n<li>Rtn. Er. Suresh Pd. Shrestha PHF 2010/11</li>\r\n<li>Rtn. Bishnu Hari Timilsina MPHF 2011/12</li>\r\n<li>Rtn Raju Paudel PHF 2012/13</li>\r\n<li>Rtn Narayan Pd. Sharma 2013/14</li>\r\n<li>Rtn Ramakanta Baral 2014/15</li>\r\n</ul>\r\n<br /><br /></div>\r\n<div>\r\n<h2>Members of the Club as April 2012 with MPHF and PHF</h2>\r\n<br />\r\n<ul>\r\n<li>1) PP Rtn. Dr. Sunu Dulal MPHF 6024267</li>\r\n<li>2) PP Rtn. Arjun Thapa PHF 6025207</li>\r\n<li>3) PP Rtn Bhuwansing Gurung PHF 6025176</li>\r\n<li>4) PP Rtn. Binod Koirala PHF 6024268</li>\r\n<li>5) PP Rtn. Jayendra Gauchan PHF 6025182</li>\r\n<li>6) IPP. Rtn. Er.Suresh Pd. Shrestha PHF 6025192</li>\r\n<li>7) President Rtn. Bishnu Hari Timilsina MPHF 6025173</li>\r\n<li>8) Rtn. Tej Bahadur Gurung 6025213</li>\r\n<li>9) Rtn. Maheswar Pahari 6025212</li>\r\n<li>10) Rtn. Baburam Pathak 6025158</li>\r\n<li>11) Rtn. Baikuntha Raj Timilsina 6025205</li>\r\n<li>12) Rtn. Ishwori Prasad Sharma 6025180</li>\r\n<li>13) Rtn. Om Prakash Gurung 6025223</li>\r\n<li>14) Rtn. Deepak Sherchan PHF 6025209</li>\r\n<li>15) Rtn. Takman Pradhananga 6025196</li>\r\n<li>16) Rtn. Raju Paudel President Elect 6266997</li>\r\n<li>17) Rtn. Naryan Pd. Sharma President Nominee PHF 6266996</li>\r\n<li>18) Rtm. Ramakanta Barala PHF 6445793</li>\r\n<li>19) Rtm. Er. Bishnu Raj Koirala PHF 7021199</li>\r\n<li>20) Rtn. Jagan Subba Gurung PHF 6713387</li>\r\n<li>21) Rtn. Rekha Karki PHF 6793193</li>\r\n<li>22) Rtn. Narendra Pd limbu PHF 8052481</li>\r\n<li>23) Rtn. Shesh bahadur Thapa 6621165</li>\r\n<li>24) Rtn. Roshani Koirala 6445768</li>\r\n<li>25) Rtn. Dr. Anil Babu Parajuli 8002485</li>\r\n<li>26) Rtn. Ashok Chalise 6894606</li>\r\n<li>27) Rtn. Nar bahadur Gurung 6445792</li>\r\n<li>28) Rtn. Prof Dr. Keshav Datta Awasthi 6532203</li>\r\n<li>29) Rtn. Shiva Prasad Parajuli 6793192</li>\r\n<li>30) Rtn. Om Raj Paudel 7021194</li>\r\n<li>31) Rtn. Dr. Suman Lal Shrestha 8052479</li>\r\n<li>32) Rtn. Douglas Maclagan 8184876</li>\r\n<li>33) Rtn. Ramesh Koirala 8245347</li>\r\n<li>34) Rtn. Rabindra K.C. 8272169</li>\r\n<li>35) Rtn. Damodar Basaula 8245351</li>\r\n<li>36) Rtn. Bal Bahadur Thapa 8245350</li>\r\n<li>37) Rtn. Dr. Niraj Kumar Dubey 8270934</li>\r\n<li>38) Rtn. Laxmi Thapa Giri 8326502</li>\r\n<li>39) Rtn. Dr. Prem Thapa 8200230</li>\r\n<li>40) Rtn. Chandra Badadur Subedi 8426236</li>\r\n</ul>\r\n</div>\r\n<p>&nbsp;</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-09-03 22:09:24', '2015-09-03 18:24:53'),
	(193, 46, 'Rotary Community Corps, dhunga sanghu, pokhara', NULL, 'rotary-community-corps-dhunga-sanghu-pokhara', '<p>A Rotary Community Corps (RCC) is a team of non-Rotarian men and women who are committed to their community\'s long-term development and selfsufficiency. An RCC is sponsored by a Rotary club and, like Rotaract and Interact clubs, is one of Rotary\'s partners in service. The main principle of the program is to enable RCC members to personally address and solve problems in the area where they work or live.</p>\r\n<h4>Consider what an RCC can do in your community:</h4>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Establish a literacy center or tutor group at a local school or library</li>\r\n<li>Set up a clinic for people who cannot afford regular health and dental care</li>\r\n<li>Create a vocational training program to help workers obtain valuable skills</li>\r\n<li>Organize teams to clean up local parks and highways</li>\r\n<li>Develop neighborhood safety projects</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h3>Program Goals</h3>\r\n<p>All RCCs share four major goals:</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>To encourage individuals to take responsibility for the improvement of their village, neighborhood, or community</li>\r\n<li>To recognize the dignity and value of all useful occupations</li>\r\n<li>To mobilize self-help activities and collective work to improve the quality of life</li>\r\n<li>To encourage the development of human potential to its fullest, within the context of the local culture and community</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 1, NULL, 'deepak_acharya.jpg', 'Deepak Acharya, Rotary Community Corps, dhunga sanghu - pokhara, President ,RI Year 2014/15', NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-09-04 00:17:01', '2015-09-03 20:46:12'),
	(194, 46, 'Interact Club of Prativa Pokhara Fishtail', NULL, 'interact-club-of-prativa-pokhara-fishtail', '<p>Interect Club of Prativa Pokkhara Fishtail was Chartered on 14 December 2012, as an independent organizatio in Prativa Heigher Seconday School, Pokhara. This club is sponsored by the Rotary Club of Pokhara Fishtail as its one of the Service Partners. The club consist of 12 to 18 years age group student both boys and girls from a single School. The charter-president of the club is Mina Pun Magar. The club holds its regular club meeting on Friday at 2 PM every fortnight at the school premises except examination time and holidayas.</p>\r\n<p>The club mainly focuses on the following parts:</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Developing leadership skills and personal integrity.</li>\r\n<li>Demonstrating helpfulness and respect for others.</li>\r\n<li>Understanding the value of individual responsibility and hardwork.</li>\r\n<li>Advancing international understanding and goodwill.</li>\r\n</ul>\r\n<p>&nbsp;</p>', 1, NULL, 'mina_pun_magar.jpg', 'Intr. Mina Pun Magar Interact Club of Prativa Pokhara Fishtail, President RI Year 2014/15', NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-09-04 09:16:05', NULL),
	(195, 46, 'Rotaract Club of Pokhara Fishtail', NULL, 'rotaract-club-of-pokhara-fishtail', '<p>Rotaract Club of Pokhara Fishtail is a chartered social female youth initiated non profit making international organisation aged 18-30 in corporated under the Rotary Club of Pokhara Fishtail.The club was chartered in 29th June 2007.Since its very inception,the club has been enthusiatically and continuously involved in various genuine activities that are highly fruitful to the soceity. The "3 H " health, hunger , humanity along with different awareness camp, personality development of club members that best serves the purpose of the club has always been the priority. Besides social service and volunteering projects intellectual development is also one of the Rotary club\'s objectives and the rotaractors are partner in service in this major focus areas.</p>\r\n<p>The height of social stigma being a pre dominant factor is surging in absence of awareness and participation and rotaract club of pokhara fishtail being a youth club makes an different attempt to shed a light in how societal dynamics can bring a structural as well as functional changes in society.</p>\r\n<p>Rotaract was founded to develop effective leaders and promote responsible citizenship. As our generation plays a growing role in directing the affairs of this community, it is important that our members be well-positioned to accept the challenge of helping Fishtail reach its potential of becoming a world-class city. We believe for those who are led to think boldly and affect real change, Rotaract provides the premier leadership development and opportunity.</p>', 1, NULL, 'chadani_grg.jpg', 'Rtr. Chadani Gurung Rotaract Club of Pokhara Fishtail, President RI Year 2014/15', NULL, NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, '2015-09-04 09:17:01', NULL),
	(196, 47, 'Scholorship', NULL, 'scholorship', '<p>.</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-09-04 19:58:48', '2015-09-04 16:16:38'),
	(197, 47, 'Micro Lan Scheme', NULL, 'micro-loan-scheme', '<h5>"Micro Loans enable the poor to life themselves out of poverty through enterpreneurship."</h5>\r\n<p>The Micro Loan Scheme(MIS) is an effective strategy for extending financial servivice to the poor and disadvantages groups not reached by formal financial sector. Rotary club of Pokhara Fishtail proud to launch Micro Loan with the help of Rotary District-1040 of UK. The mission of Micro Loan is to contribute towards the efforts of poverty alleviation through the sustain ability with improve livelihood of urban poor by providing loan without interest to start new small business or upgrade existing one. The club has been operating the program since 2012 by establishing a revolving fund of NRs. 300,000. By now, more than 25 people have received the loan without interest ranging from 10,000 to 20,000 rupees. Micro loan Scheme is one of the best projects initiates by the club with direct involvement of Rotarians in selection, implementation and monitoring.</p>\r\n<p>In initiation with the RC Pokhara Fishtail, RC Baglung and RC Lamjung has also started and run the Micro Loan Scheme in their respective districts.</p>\r\n<h4><a href="/cel/2015/aug/projects/rotaryfishtail/old/download.php?filename=micro_loan_scheme.pdf">For more details, click here to download Brochure</a></h4>', 1, NULL, 'micro_loan.png', 'What is a Micro Loan? The Procvision of a small loan for those who are too poor to have access to co', NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-09-04 19:59:32', '2015-09-04 17:23:26'),
	(198, 4, 'club past projects', NULL, 'club-past-projects', '<h2>Service Projects completed and handed over in 2013</h2>\r\n<p>&nbsp;</p>\r\n<h3>1. WATER SUPPLY PROJECT AT SAHARA ACADEMY</h3>\r\n<ul>\r\n<ul>\r\n<li><strong>Objectives:</strong> To assure adequate water supply for students/trainees and staff of Sahara Academy.</li>\r\n<li><strong>Partner club:</strong> Rotary Club of Cookham Bridge, UK</li>\r\n<li><strong>Status:</strong> Handed over in February 2013</li>\r\n<li><strong>Budget:</strong> NRs. 2,100,000</li>\r\n<li><strong>Committee:</strong>Tej Gurung (Coordinator) , Members: Binod Koirala, Narendra Limbu, (Er) Suresh Prasad Shrestha</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<h3>2. WATER SUPPLY TO MAJHKOT VILLAGE AND SIVALAYA SECONDARY SCHOOL OF THUMKI</h3>\r\n<ul>\r\n<ul>\r\n<li><strong>Objectives:</strong> To assure adequate water supply and improve Sanitation condition at Sivalaya Secondary School and Majhkot village of Thumki VDC, Kaski.</li>\r\n<li><strong>Partner club:</strong> Rotary Club of Bingley Airedale, UK</li>\r\n<li><strong>Status:</strong> Completed and handed over to the community on 28 February 2013</li>\r\n<li><strong>Budget:</strong> Rs. 1,400,000</li>\r\n<li><strong>Committee:</strong>Binod Koirala (Coordinator) , MembersNarendra Limbu, Suresh Prasad Shrestha and Jayendra Gauchan</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<h3>3. WATER SUPPLY TO DHRUBA HIGHER SECONDARY SCHOOL, TARKANG, KASKI</h3>\r\n<ul>\r\n<li><strong>Objectives:</strong> adequate drinking water to students and teachers of the school and improve sanitation condition</li>\r\n<li><strong>Partner club:</strong> RC Grovedale, Australia</li>\r\n<li><strong>Status:</strong> the project and handed over the School Management Committee on 5th May 2013.</li>\r\n<li><strong>Budget:</strong> NRs. 300,000 with additional contribution from locals</li>\r\n<li><strong>Committee:</strong> Binod Koirala (Coordinator), Members: Bishnuhari Timillsina, Narendra Limbu and Jayendra Gauchan</li>\r\n</ul>\r\n<p><br /><br /></p>\r\n<h2>Milestone Projects carried out by the club.</h2>\r\n<p>Since the time of establishment it has contributed in the field of health, education, environment &amp; humanity. Some key important achievements among them were.<br /><br /></p>\r\n<ul>\r\n<ul>\r\n<li>1) Toilet and Drinking water project completed in Janata Primary school, Armala kaski with partner club RC Maing of Germany NRs. 1,75000 (2004/05)</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>2) Bir Primary School of Ritthepani, Lekhnath Kaski Toilet Programme NRS 2,25000/- Partner Club Rc South borough and Penburg U.K.</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>3) Srijana Drinking Water Project : The project was completed 2009/10 at Pokhara-17. Total Cost : NRS 18 Lakhs partner club RC Breukelum Vechtstreet, Netherland.</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>4) Wheel Chair Distributing: 100 wheelchair were distributed to disable people in different areas of Pokhara, Syangja, Waling, Myagdi Tanahun, Lamjung and many other places by matching grants project (2011/12).</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>5) Ambulance handed over to Nepal Red Cross Society, Kaski, Chapter on Sept. 2011 Partner Club - RC Surrey B.C. Canada &amp; RC Grovedale Australia by matching grants project.</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>6) Kolma Drinking Water Supply Project : Project handed over to community on 3rd Dec 2011, Total cost NRs.11 Lakhs Partner Club - Rotary Club Bristol U.K.</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>7) Sahara Drinking Water Project : Project under construction at Pokhara-17 for Sahara Academy- orphan children. Amount NRS 2216000.-partner club RC Kookham Bridge UK</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<ul>\r\n<li>8) Majhkot Shivalaya Thumki water supply project on going implementing partner club- Rotary Club of Bingley Airdale U.K. Project Cost : NRs.13 Lakhs by matching grants project</li>\r\n</ul>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>9) Prolapse Surgery Project : With the initiation of the club and collaboration of Nepal Government (WRHD) conducted prolapse uterus surgery camp in remote rural area of western region at Bhimad Primary Health Center. In this camp 156 women directly benefited with prolapse surgery.</li>\r\n</ul>\r\n<p>&nbsp;</p>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-09-04 20:13:23', '2015-09-04 17:38:41'),
	(199, 4, 'BULLETINS OF THE YEAR 2014/15', NULL, 'bulletins-of-the-year-201415', '<p>Weekly Meeting Minutes</p>\r\n<p><a title="hora" href="/cel/2015/aug/projects/rotaryfishtail/new/article/uploads/tinyMCE/main/gallery/_T5A2278.jpg">Relief Distribution</a></p>\r\n<div>\r\n<h3><a href="/cel/2015/aug/projects/rotaryfishtail/old/download.php?filename=Issue I May.pdf">RELIEF DISTRIBUTION</a></h3>\r\n</div>\r\n<div>\r\n<h3><a href="/cel/2015/aug/projects/rotaryfishtail/old/download.php?filename=Issue II November.pdf">NOVEMBER ISSUE II</a></h3>\r\n</div>\r\n<div>\r\n<h3><a href="/cel/2015/aug/projects/rotaryfishtail/old/download.php?filename=Issue_I_November.pdf">NOVEMBER ISSUE</a></h3>\r\n</div>', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 25, NULL, NULL, NULL, NULL, NULL, '2015-09-04 22:45:06', '2015-09-04 19:04:53');
/*!40000 ALTER TABLE `tbl_articles` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_campaign
CREATE TABLE IF NOT EXISTS `tbl_campaign` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `user_id` int(22) NOT NULL,
  `fund_category_id` int(22) DEFAULT NULL,
  `campaign_title` varchar(111) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `target_amount` int(11) NOT NULL,
  `description` text NOT NULL,
  `pic` varchar(25000) NOT NULL,
  `video` varchar(111) NOT NULL,
  `document` varchar(111) DEFAULT NULL,
  `fb_link` varchar(111) NOT NULL,
  `twitter_link` varchar(111) NOT NULL,
  `enable_comment` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `starting_at` date NOT NULL,
  `ending_at` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `url_link` varchar(50) DEFAULT NULL,
  `success_story` varchar(20000) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `fund_category_id` (`fund_category_id`),
  CONSTRAINT `FK_tbl_campaign_tbl_users` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_campaign: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_campaign` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_campaign` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_categories
CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(25) NOT NULL,
  `content` varchar(2000) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `image_title` varchar(50) DEFAULT NULL,
  `url` varchar(100) NOT NULL,
  `order` int(10) unsigned DEFAULT NULL,
  `published` tinyint(3) unsigned DEFAULT NULL,
  `author` int(10) DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`name`),
  KEY `tbl_categories_ibfk_1` (`parent_id`),
  KEY `tbl_categories_ibfk_2` (`author`),
  KEY `tbl_categories_ibfk_3` (`modified_by`),
  CONSTRAINT `FK_tbl_categories_tbl_categories` FOREIGN KEY (`parent_id`) REFERENCES `tbl_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_categories_tbl_users` FOREIGN KEY (`author`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_categories_tbl_users_2` FOREIGN KEY (`modified_by`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_categories: ~11 rows (approximately)
/*!40000 ALTER TABLE `tbl_categories` DISABLE KEYS */;
INSERT INTO `tbl_categories` (`id`, `parent_id`, `name`, `slug`, `content`, `image`, `image_title`, `url`, `order`, `published`, `author`, `modified_by`, `created_at`, `updated_at`, `status`) VALUES
	(4, NULL, 'uncategoried', 'uncategoried', 'uncategoried', '', '', '', 1, 1, 25, NULL, '2015-03-01 12:17:53', '2015-07-25 11:52:26', 1),
	(38, NULL, 'Help', 'help', 'help', '', '', '', 1, 1, 25, NULL, '2015-03-01 12:17:53', '2015-07-25 11:52:26', 2),
	(39, NULL, 'messages', 'messages', 'help', '', '', '', 1, 1, 25, NULL, '2015-03-01 12:17:53', '2015-07-25 11:52:26', 1),
	(40, NULL, 'projects', 'projects', 'projects', '', '', '', 1, 1, 25, NULL, '2015-03-01 12:17:53', '2015-07-25 11:52:26', 1),
	(41, NULL, 'news & events', 'news', 'news & events', '179.jpg', '', '', 1, 1, 25, NULL, '2015-03-01 12:17:53', '2015-07-25 11:52:26', 1),
	(42, NULL, 'Aubrey Kline', 'aubrey-kline', '<p>e</p>', '235397-1408974243.gif', 'Possimus voluptate est odio sed veniam esse vitae ', 'Occaecat corrupti enim qui minim delenitie', NULL, NULL, 25, NULL, '2015-08-29 13:37:41', NULL, 2),
	(43, NULL, 'Robert Cranen', 'robert-cranen', '<p>cdfdf</p>', 'angular-forms-300x125.jpg', 'itAnimi voluptatem Veniam explicabo Inventore et q', 'uCulpa ea asperiores dolore autem quod aliquam non harum culpa eum voluptate', NULL, NULL, 25, NULL, '2015-08-29 13:55:06', NULL, 2),
	(44, NULL, 'Renee Howe', 'renee-howe', '<p>e</p>', '', 'Odit fuga Et quia minima rerum', 'Consequatur Accusamus sunt corporis et ducimus nulla deserunt proident sed', NULL, NULL, 25, NULL, '2015-08-29 16:01:56', NULL, 2),
	(45, NULL, 'slider', 'slider', '', '', '', '', NULL, NULL, 25, NULL, '2015-09-01 19:49:52', NULL, 1),
	(46, NULL, 'club_wings', 'club_wings', '<p>Service Partner of Rotary Club</p>', '', '', '', NULL, NULL, 25, NULL, '2015-09-03 22:13:57', NULL, 1),
	(47, NULL, 'long term project', 'long-term-project', '<p>Service Partner of Rotary Club</p>', '', '', '', NULL, NULL, 25, NULL, '2015-09-04 19:58:06', NULL, 1);
/*!40000 ALTER TABLE `tbl_categories` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_donar
CREATE TABLE IF NOT EXISTS `tbl_donar` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_donar: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_donar` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_donar` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_donation
CREATE TABLE IF NOT EXISTS `tbl_donation` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `donar_id` int(22) NOT NULL,
  `campaign_id` int(22) NOT NULL,
  `amount` int(22) NOT NULL,
  `comment` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `donar_id` (`donar_id`),
  KEY `campaign_id` (`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_donation: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_donation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_donation` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_fund_categories
CREATE TABLE IF NOT EXISTS `tbl_fund_categories` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `name` varchar(111) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `glyphicon` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_fund_categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_fund_categories` DISABLE KEYS */;
INSERT INTO `tbl_fund_categories` (`id`, `name`, `slug`, `description`, `image`, `glyphicon`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'fund_category', 'fund_category', 'Provident, nesciunt, a cupiditate praesentium ut aut maxime reprehenderit, quia culpa quis repudiandae eiusmod quia pariatur? Nisi.', '', 'fa fa-heart', 1, '2015-06-27 18:06:03', '2015-07-01 04:57:41'),
	(2, 'Jaime Gibson', 'jaime-gibson', 'Sint nisi minima sit elit, sed tenetur irure duis aliquid velit quis saepe aliquam et enim aut nesciunt, deleniti.', '', 'Voluptas eius commodi facere dolor', 2, '2015-07-15 19:26:57', NULL);
/*!40000 ALTER TABLE `tbl_fund_categories` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_groups
CREATE TABLE IF NOT EXISTS `tbl_groups` (
  `id` int(22) unsigned NOT NULL AUTO_INCREMENT,
  `parent_group_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `tbl_groups_ibfk_1` (`parent_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_groups: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_groups` DISABLE KEYS */;
INSERT INTO `tbl_groups` (`id`, `parent_group_id`, `name`, `slug`, `desc`, `status`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Superadmin', 'superadmin', 'superadmin desc', 1, '2015-01-28 00:36:41', NULL),
	(2, 1, 'Admin', 'admin', '', 1, '2015-01-28 00:37:01', NULL),
	(3, NULL, 'Donee', 'donee', 'Quibusdam at sint voluptas eum debitis accusamus voluptatem voluptatem, non eu et in et illum, ab et sed ut.', 1, '2015-03-28 13:22:30', NULL),
	(4, NULL, 'Facebook User', 'facebook_user', 'd\r\n', 1, '2015-05-19 17:00:54', NULL);
/*!40000 ALTER TABLE `tbl_groups` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_group_permissions
CREATE TABLE IF NOT EXISTS `tbl_group_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned DEFAULT NULL,
  `permission_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_group_permissions_ibfk_1` (`group_id`),
  KEY `tbl_group_permissions_ibfk_2` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3780 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_group_permissions: ~79 rows (approximately)
/*!40000 ALTER TABLE `tbl_group_permissions` DISABLE KEYS */;
INSERT INTO `tbl_group_permissions` (`id`, `group_id`, `permission_id`, `created_at`, `updated_at`) VALUES
	(1749, 8, 80, '2015-06-08 15:07:15', NULL),
	(1750, 8, 82, '2015-06-08 15:07:15', NULL),
	(1751, 8, 83, '2015-06-08 15:07:15', NULL),
	(1752, 8, 84, '2015-06-08 15:07:15', NULL),
	(1753, 8, 85, '2015-06-08 15:07:15', NULL),
	(1754, 8, 86, '2015-06-08 15:07:15', NULL),
	(3665, 2, 5, '2015-06-27 21:42:35', NULL),
	(3666, 2, 6, '2015-06-27 21:42:35', NULL),
	(3667, 2, 7, '2015-06-27 21:42:35', NULL),
	(3668, 2, 96, '2015-06-27 21:42:35', NULL),
	(3669, 2, 97, '2015-06-27 21:42:35', NULL),
	(3670, 2, 98, '2015-06-27 21:42:35', NULL),
	(3671, 2, 99, '2015-06-27 21:42:35', NULL),
	(3672, 2, 55, '2015-06-27 21:42:35', NULL),
	(3673, 2, 61, '2015-06-27 21:42:35', NULL),
	(3674, 2, 64, '2015-06-27 21:42:35', NULL),
	(3675, 2, 65, '2015-06-27 21:42:35', NULL),
	(3676, 2, 66, '2015-06-27 21:42:36', NULL),
	(3677, 2, 67, '2015-06-27 21:42:36', NULL),
	(3678, 2, 68, '2015-06-27 21:42:36', NULL),
	(3679, 2, 70, '2015-06-27 21:42:36', NULL),
	(3680, 2, 72, '2015-06-27 21:42:36', NULL),
	(3681, 2, 73, '2015-06-27 21:42:36', NULL),
	(3682, 2, 74, '2015-06-27 21:42:36', NULL),
	(3683, 2, 76, '2015-06-27 21:42:36', NULL),
	(3684, 2, 77, '2015-06-27 21:42:36', NULL),
	(3685, 2, 78, '2015-06-27 21:42:36', NULL),
	(3686, 2, 79, '2015-06-27 21:42:36', NULL),
	(3687, 2, 80, '2015-06-27 21:42:36', NULL),
	(3688, 2, 82, '2015-06-27 21:42:36', NULL),
	(3689, 2, 83, '2015-06-27 21:42:36', NULL),
	(3690, 2, 84, '2015-06-27 21:42:36', NULL),
	(3691, 2, 85, '2015-06-27 21:42:36', NULL),
	(3692, 2, 86, '2015-06-27 21:42:36', NULL),
	(3693, 2, 100, '2015-06-27 21:42:36', NULL),
	(3694, 2, 91, '2015-06-27 21:42:36', NULL),
	(3737, 1, 5, '2015-07-31 22:40:16', NULL),
	(3738, 1, 6, '2015-07-31 22:40:16', NULL),
	(3739, 1, 7, '2015-07-31 22:40:16', NULL),
	(3740, 1, 96, '2015-07-31 22:40:16', NULL),
	(3741, 1, 97, '2015-07-31 22:40:16', NULL),
	(3742, 1, 98, '2015-07-31 22:40:16', NULL),
	(3743, 1, 99, '2015-07-31 22:40:16', NULL),
	(3744, 1, 8, '2015-07-31 22:40:17', NULL),
	(3745, 1, 9, '2015-07-31 22:40:17', NULL),
	(3746, 1, 10, '2015-07-31 22:40:17', NULL),
	(3747, 1, 17, '2015-07-31 22:40:17', NULL),
	(3748, 1, 15, '2015-07-31 22:40:17', NULL),
	(3749, 1, 16, '2015-07-31 22:40:17', NULL),
	(3750, 1, 87, '2015-07-31 22:40:17', NULL),
	(3751, 1, 88, '2015-07-31 22:40:17', NULL),
	(3752, 1, 89, '2015-07-31 22:40:17', NULL),
	(3753, 1, 90, '2015-07-31 22:40:17', NULL),
	(3754, 1, 92, '2015-07-31 22:40:17', NULL),
	(3755, 1, 93, '2015-07-31 22:40:17', NULL),
	(3756, 1, 55, '2015-07-31 22:40:17', NULL),
	(3757, 1, 61, '2015-07-31 22:40:17', NULL),
	(3758, 1, 64, '2015-07-31 22:40:17', NULL),
	(3759, 1, 65, '2015-07-31 22:40:17', NULL),
	(3760, 1, 66, '2015-07-31 22:40:17', NULL),
	(3761, 1, 67, '2015-07-31 22:40:17', NULL),
	(3762, 1, 68, '2015-07-31 22:40:17', NULL),
	(3763, 1, 70, '2015-07-31 22:40:17', NULL),
	(3764, 1, 72, '2015-07-31 22:40:17', NULL),
	(3765, 1, 73, '2015-07-31 22:40:18', NULL),
	(3766, 1, 74, '2015-07-31 22:40:18', NULL),
	(3767, 1, 76, '2015-07-31 22:40:18', NULL),
	(3768, 1, 77, '2015-07-31 22:40:18', NULL),
	(3769, 1, 78, '2015-07-31 22:40:18', NULL),
	(3770, 1, 79, '2015-07-31 22:40:18', NULL),
	(3771, 1, 101, '2015-07-31 22:40:18', NULL),
	(3772, 1, 80, '2015-07-31 22:40:18', NULL),
	(3773, 1, 82, '2015-07-31 22:40:18', NULL),
	(3774, 1, 83, '2015-07-31 22:40:18', NULL),
	(3775, 1, 84, '2015-07-31 22:40:18', NULL),
	(3776, 1, 85, '2015-07-31 22:40:18', NULL),
	(3777, 1, 86, '2015-07-31 22:40:18', NULL),
	(3778, 1, 100, '2015-07-31 22:40:18', NULL),
	(3779, 1, 91, '2015-07-31 22:40:18', NULL);
/*!40000 ALTER TABLE `tbl_group_permissions` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_logs
CREATE TABLE IF NOT EXISTS `tbl_logs` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `user_id` int(22) NOT NULL,
  `log_type` varchar(111) NOT NULL,
  `log_description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_logs` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_menus
CREATE TABLE IF NOT EXISTS `tbl_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `page_type_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `article_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `level` int(11) DEFAULT '0',
  `url` varchar(200) DEFAULT NULL,
  `order` int(10) unsigned DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `author` int(10) DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `sidebar` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `tbl_menus_ibfk_1` (`parent_id`),
  KEY `tbl_menus_ibfk_2` (`author`),
  KEY `tbl_menus_ibfk_3` (`modified_by`),
  KEY `tbl_menus_ibfk_4` (`page_type_id`),
  KEY `tbl_menus_ibfk_5` (`category_id`),
  KEY `FK_tbl_menus_tbl_articles` (`article_id`),
  CONSTRAINT `FK_tbl_menus_tbl_articles` FOREIGN KEY (`article_id`) REFERENCES `tbl_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_categories` FOREIGN KEY (`category_id`) REFERENCES `tbl_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_menus` FOREIGN KEY (`parent_id`) REFERENCES `tbl_menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_page_types` FOREIGN KEY (`page_type_id`) REFERENCES `tbl_page_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_users` FOREIGN KEY (`author`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_menus_tbl_users_2` FOREIGN KEY (`modified_by`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_menus: ~34 rows (approximately)
/*!40000 ALTER TABLE `tbl_menus` DISABLE KEYS */;
INSERT INTO `tbl_menus` (`id`, `parent_id`, `page_type_id`, `category_id`, `article_id`, `name`, `level`, `url`, `order`, `slug`, `desc`, `status`, `author`, `modified_by`, `created_at`, `updated_at`, `sidebar`) VALUES
	(1, NULL, 1, NULL, NULL, 'Home', 0, 'home', 1, 'home', 'home desc', 2, 25, NULL, '2015-03-02 02:24:54', NULL, 'N'),
	(10, 15, 2, NULL, NULL, 'page dfd', 0, NULL, 9, 'page-1', '', 4, 25, NULL, '2015-03-03 02:36:03', NULL, NULL),
	(11, 10, 2, 4, NULL, 'page one', 1, NULL, 10, 'page-11', '', 4, 25, NULL, '2015-03-03 02:36:24', NULL, NULL),
	(13, 10, 2, 4, NULL, 'page one two', 1, NULL, 13, 'page-12', '', 2, 25, NULL, '2015-03-03 02:36:54', NULL, NULL),
	(14, 11, 2, NULL, NULL, 'page 1.1.1', 2, NULL, 11, 'page-111', '', 2, 25, NULL, '2015-03-03 02:37:16', NULL, NULL),
	(15, 39, 5, 47, NULL, 'Long-term Project', 1, 'long_term_project', 11, 'page-121', '', 2, 25, NULL, '2015-03-03 02:37:38', NULL, 'N'),
	(16, 13, 2, NULL, NULL, 'page 1.2.2', 2, NULL, 15, 'page-122', '', 2, 25, NULL, '2015-03-03 02:38:04', NULL, NULL),
	(17, NULL, 1, NULL, NULL, 'n', 0, NULL, 1, 'n', 'D', 4, 25, NULL, '2015-03-14 00:54:48', NULL, 'Y'),
	(18, NULL, 9, NULL, NULL, 'about us', 0, 'about-us', 2, 'about-us', '', 2, 25, NULL, '2015-03-16 21:32:55', NULL, 'N'),
	(19, NULL, 2, NULL, NULL, 'Club Members', 0, NULL, 7, 'courses', '', 2, 25, NULL, '2015-03-16 21:33:11', NULL, 'N'),
	(25, NULL, 1, NULL, 199, 'Bulletins', 0, 'bulletins', 15, 'admission', '', 2, 25, NULL, '2015-03-16 21:37:14', NULL, 'N'),
	(26, 18, 1, NULL, 54, 'Mission & Objectives', 1, 'mission_objectives', 4, 'services', '', 2, 25, NULL, '2015-03-16 21:37:27', NULL, 'Y'),
	(27, NULL, 4, 41, NULL, 'News', 0, 'news', 14, 'news-events', '', 2, 25, NULL, '2015-03-16 21:37:41', NULL, 'Y'),
	(28, 14, 1, NULL, NULL, 'page 1.1.1.1', 3, NULL, 12, 'page-1111', '', 2, 25, NULL, '2015-03-16 22:01:34', NULL, NULL),
	(29, 19, 2, NULL, NULL, 'Board of Directors', 1, NULL, 8, 'diploma-level-courses', '', 2, 25, NULL, '2015-03-19 22:35:19', NULL, 'N'),
	(31, 19, 2, 40, NULL, 'Active Members', 1, NULL, 9, 'advanced-level-courses', '', 2, 25, NULL, '2015-03-19 22:35:51', NULL, 'N'),
	(32, 19, 1, 43, NULL, 'Courses 3', 1, NULL, 16, 'courses-3', '', 1, 25, NULL, '2015-03-20 00:18:53', NULL, NULL),
	(33, NULL, 3, NULL, NULL, 'Gallery', 0, 'gallery', 13, 'gallery', '', 2, 25, NULL, '2015-03-20 00:45:09', NULL, 'N'),
	(35, 39, 1, NULL, 198, 'Past Projects', 1, 'project_past', 12, 'downloads', '', 2, 25, NULL, '2015-03-20 00:48:57', NULL, 'Y'),
	(36, NULL, 6, NULL, NULL, 'Contact', 0, 'contact', 16, 'contact', '', 2, 25, NULL, '2015-03-20 00:58:08', NULL, 'Y'),
	(37, 18, 1, NULL, 192, 'Club Profile', 1, 'clubprofile', 5, 'new-menu', '', 2, 25, NULL, '2015-03-28 13:10:50', NULL, 'Y'),
	(38, NULL, 5, 46, NULL, 'Club\'s Wings', 0, 'club_wings', 6, 'chip-level-courses', '', 2, 25, NULL, '2015-03-29 16:22:06', NULL, 'N'),
	(39, NULL, 9, NULL, NULL, 'Service Projects', 0, 'service_projects', 10, 'cretification-level-courses', '', 2, 25, NULL, '2015-03-29 16:22:20', NULL, 'N'),
	(40, 40, 7, 41, 182, 'nabout chals', 0, NULL, 15, 'about-chals', 'm', 2, 25, NULL, '2015-05-24 20:31:10', NULL, NULL),
	(41, 46, 7, 40, 166, 'Laura Carson', 1, NULL, 18, 'laura-carson', 'Occaecat repellendus. Accusamus quo sed praesentium eos voluptatem. Eius numquam cupidatat quas tenetur minim.', 2, 25, NULL, '2015-08-30 23:05:53', NULL, NULL),
	(42, NULL, 7, 40, 166, 'Laura Carsone', 0, NULL, 25, 'laura-carsone', 'Occaecat repellendus. Accusamus quo sed praesentium eos voluptatem. Eius numquam cupidatat quas tenetur minim.', NULL, 25, NULL, '2015-08-30 23:16:25', NULL, NULL),
	(43, 18, 6, 38, 64, 'Diana Brock', 1, NULL, 25, 'diana-brock', 'Et itaque saepe enim fugiat, repudiandae eum laboris sit sint, quod odit atque sint.', NULL, 25, NULL, '2015-08-30 23:19:56', NULL, NULL),
	(44, NULL, 3, 38, 64, 'Emma Acevedo', 2, NULL, 18, 'emma-acevedo', 'Soluta odit qui porro dolorem aut duis consequatur? Accusamus minim aut ex fugit, et tempore, aut voluptate aute eligendi voluptas.', 4, 25, NULL, '2015-08-30 23:20:26', NULL, NULL),
	(45, 46, 4, 41, 166, 'Iliana Roberts', 1, NULL, 19, 'iliana-roberts', 'Sit molestias maxime culpa, quo veritatis numquam et reiciendis voluptatem irure.', 2, 25, NULL, '2015-08-30 23:21:00', NULL, NULL),
	(46, NULL, 2, 4, NULL, 'new pages', 0, NULL, 17, 'new-pages', '', 3, 25, NULL, '2015-08-30 23:51:47', NULL, 'N'),
	(47, 18, 1, NULL, 51, 'introduction', 1, 'our-introduction', 3, 'introduction', 'Ut fugiat, dolor eligendi iste ullamco voluptas beatae laboris pariatur? Repellendus. Non consequatur quos doloribus.', 2, 25, NULL, '2015-08-31 01:02:37', NULL, 'Y'),
	(48, 46, 7, 39, 164, 'Zephr Simpson', 1, NULL, 20, 'zephr-simpson', 'Consequatur a voluptas ea doloribus voluptas soluta consequatur, inventore veniam, sit, qui nulla blanditiis voluptate sapiente magni.', 2, 25, NULL, '2015-08-31 01:03:10', NULL, 'N'),
	(49, 46, 2, 39, 161, 'Ingrid Mann', 1, NULL, 21, 'ingrid-mann', 'Iste perspiciatis, perferendis officia sint, sit, consequatur? Exercitation dolorum adipisicing dolore et exercitationem esse, nobis aliquam consequatur facilis.', 2, 25, NULL, '2015-08-31 01:09:15', NULL, 'N'),
	(50, NULL, 1, 39, 166, 'Rajah Navarro', 0, 'about-us-n', 31, 'rajah-navarro', '', 4, 25, NULL, '2015-09-03 19:58:23', NULL, 'N');
/*!40000 ALTER TABLE `tbl_menus` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_page_types
CREATE TABLE IF NOT EXISTS `tbl_page_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_page_types: ~9 rows (approximately)
/*!40000 ALTER TABLE `tbl_page_types` DISABLE KEYS */;
INSERT INTO `tbl_page_types` (`id`, `name`, `desc`) VALUES
	(1, 'article', NULL),
	(2, 'category', NULL),
	(3, 'gallery', NULL),
	(4, 'news', NULL),
	(5, 'tabs', NULL),
	(6, 'contact', NULL),
	(7, 'timeline', NULL),
	(8, '404', NULL),
	(9, 'blank', NULL);
/*!40000 ALTER TABLE `tbl_page_types` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_permissions
CREATE TABLE IF NOT EXISTS `tbl_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_permission_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fk_parent_permission_id` (`parent_permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_permissions: ~62 rows (approximately)
/*!40000 ALTER TABLE `tbl_permissions` DISABLE KEYS */;
INSERT INTO `tbl_permissions` (`id`, `parent_permission_id`, `name`, `slug`, `desc`, `created_at`) VALUES
	(5, NULL, 'Administrator User', 'administrator-user', '', '2015-01-29 00:56:32'),
	(6, 5, 'add user', 'add-user', '', '2015-01-29 00:58:23'),
	(7, 5, 'edit user', 'edit-user', '', '2015-01-29 00:58:38'),
	(8, NULL, 'Administrator  Permission', 'administrator-permission', '', '2015-01-29 00:59:51'),
	(9, 8, 'add Permission', 'add-permission', '', '2015-01-29 01:00:02'),
	(10, 8, 'edit Permission', 'edit-permission', '', '2015-01-29 01:00:11'),
	(15, NULL, 'Administrator Group', 'administrator-group', '', '2015-02-01 15:40:08'),
	(16, 15, 'Add Group', 'add-group', '', '2015-02-01 15:45:09'),
	(17, 8, 'delete Permission', 'delete-permission', '', '2015-02-20 22:21:45'),
	(31, 30, 'list category', 'list-category', '', '2015-02-23 01:40:17'),
	(32, 30, 'add category', 'add-category', '', '2015-02-23 01:43:00'),
	(33, 30, 'edit category', 'edit-category', '', '2015-02-23 01:43:17'),
	(34, 30, 'delete category', 'delete-category', '', '2015-02-23 01:43:27'),
	(35, 30, 'activate category', 'activate-category', '', '2015-02-23 01:43:35'),
	(36, 30, 'block category', 'block-category', '', '2015-02-23 01:43:43'),
	(38, 37, 'list menu', 'list-menu', '', '2015-02-23 01:40:17'),
	(39, 37, 'add menu', 'add-menu', '', '2015-02-23 01:43:00'),
	(40, 37, 'edit menu', 'edit-menu', '', '2015-02-23 01:43:17'),
	(41, 37, 'delete menu', 'delete-menu', '', '2015-02-23 01:43:27'),
	(42, 37, 'activate menu', 'activate-menu', '', '2015-02-23 01:43:35'),
	(43, 37, 'block menu', 'block-menu', '', '2015-02-23 01:43:43'),
	(48, 47, 'add article', 'add-article', '', '2015-02-23 01:43:00'),
	(49, 47, 'edit article', 'edit-article', '', '2015-02-23 01:43:17'),
	(50, 47, 'delete article', 'delete-article', '', '2015-02-23 01:43:27'),
	(51, 47, 'activate article', 'activate-article', '', '2015-02-23 01:43:35'),
	(52, 47, 'block article', 'block-article', '', '2015-02-23 01:43:43'),
	(53, 47, 'list article', 'list-article', '', '2015-02-23 01:40:17'),
	(54, 47, 'view article', 'view-article', '', '2015-02-23 01:40:17'),
	(55, NULL, 'administrator setting', 'administrator-setting', '', '2015-01-29 00:56:32'),
	(61, NULL, 'administrator fund category', 'administrator-fund-category', '', '2015-05-24 21:40:30'),
	(64, 61, 'add-fund-category', 'add-fund-category', '', '2015-05-24 21:40:30'),
	(65, 61, 'edit-fund-category', 'edit-fund-category', '', '2015-05-24 21:40:30'),
	(66, 61, 'delete-fund-category', 'delete-fund-category', '', '2015-05-24 21:40:30'),
	(67, 61, 'activate-fund-category', 'activate-fund-category', '', '2015-05-24 21:40:30'),
	(68, 61, 'block-fund-category', 'block-fund-category', '', '2015-05-24 21:40:30'),
	(70, 61, 'list-fund-category', 'list-fund-category', '', '2015-05-24 21:40:30'),
	(72, NULL, 'administrator-campaign', 'administrator-campaign', '', '2015-05-24 21:40:30'),
	(73, 72, 'add-campaign', 'add-campaign', '', '2015-05-24 21:40:30'),
	(74, 72, 'edit-campaign', 'edit-campaign', '', '2015-05-24 21:40:30'),
	(76, 72, 'delete-campaign', 'delete-campaign', '', '2015-05-24 21:40:30'),
	(77, 72, 'activate-campaign', 'activate-campaign', '', '2015-05-24 21:40:30'),
	(78, 72, 'block-campaign', 'block-campaign', '', '2015-05-24 21:40:30'),
	(79, 72, 'list-campaign', 'list-campaign', '', '2015-05-24 21:40:30'),
	(80, NULL, 'administrator-donee', 'administrator-donee', '', '2015-06-07 21:43:27'),
	(82, 80, 'list-donee', 'list-donee', '', '2015-06-07 21:44:53'),
	(83, 80, 'add-donee', 'add-donee', '', '2015-06-07 21:45:20'),
	(84, 80, 'activate-donee', 'activate-donee', '', '2015-06-07 21:45:43'),
	(85, 80, 'block-donee', 'block-donee', '', '2015-06-07 21:45:56'),
	(86, 80, 'delete-donee', 'delete-donee', '', '2015-06-07 21:46:07'),
	(87, 15, 'edit group', 'edit-group', '', '2015-06-08 08:51:37'),
	(88, 15, 'activate-group', 'activate-group', '', '2015-06-08 14:52:22'),
	(89, 15, 'block-group', 'block-group', '', '2015-06-08 14:52:37'),
	(90, 15, 'delete-group', 'delete-group', '', '2015-06-08 14:52:54'),
	(91, NULL, 'administrator-donation', 'administrator-donation', '', '2015-06-07 21:43:27'),
	(92, 15, 'update-permission-group', 'update-permission-group', '', '2015-06-19 09:44:57'),
	(93, 15, 'List Group', 'list-group', '', '2015-06-19 09:46:11'),
	(96, 5, 'activate user', 'activate-user', '', '2015-01-29 00:58:38'),
	(97, 5, 'block user', 'block-user', '', '2015-01-29 00:58:38'),
	(98, 5, 'delete user', 'delete-user', '', '2015-01-29 00:58:38'),
	(99, 5, 'list user', 'list-user', '', '2015-01-29 00:58:38'),
	(100, 80, 'edit-donee', 'edit-donee', '', '2015-06-20 18:40:28'),
	(101, 72, 'success-campaign', 'success-campaign', 'success-campaign desc', '2015-07-31 22:38:31');
/*!40000 ALTER TABLE `tbl_permissions` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_settings
CREATE TABLE IF NOT EXISTS `tbl_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `value` longtext NOT NULL,
  `autoload` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_settings: ~42 rows (approximately)
/*!40000 ALTER TABLE `tbl_settings` DISABLE KEYS */;
INSERT INTO `tbl_settings` (`id`, `name`, `slug`, `value`, `autoload`) VALUES
	(1, 'email', 'email', 'info@ourlibraray.com', 1),
	(2, 'cell', 'cell', '9806677215', 0),
	(3, 'address', 'address', 'Melbourne , Austrailia', 0),
	(5, 'facebook_link', 'facebook_link', 'https://www.facebook.com/rotaryfishtail.org', 0),
	(6, 'twitter_link', 'twitter_link', 'https://www.twitter.com/rotaryfishtail.org', 0),
	(7, 'google_plus_link', 'google_plus_link', 'https://www.googleplus.com/rotaryfishtail.org', 0),
	(8, 'Site Name', 'site_name', 'Rotary Club Of Pokhara Fishtail', 0),
	(9, 'Site Url', 'site_url', 'Rotary Club Of Pokhara Fishtail', 0),
	(12, 'club_name', 'club_name', 'Rotary Club Of Pokhara Fishtail', 1),
	(13, 'club_number', 'club_number', 'Club No. 63638', 1),
	(15, 'slogan_head', 'slogan_head', 'Service above Self', 1),
	(16, 'slogan_body', 'slogan_body', 'ROTARY INTERNATIONAL', 1),
	(17, 'slogan_dist', 'slogan_dist', 'Dist. 32921', 1),
	(18, 'other_site_1_name', 'other_site_1_name', 'Fishtail Fund', 1),
	(19, 'other_site_1_url', 'other_site_1_url', 'http://www.fishtailfund.com/', 1),
	(20, 'other_site_2_name', 'other_site_2_name', 'Annapurna Fund', 1),
	(21, 'other_site_2_url', 'other_site_2_url', 'http://www.fishtailfund.np/e', 1),
	(22, 'slider_show_total', 'slider_show_total', '5', 1),
	(23, 'sidebar1_content', 'sidebar1_content', 'n', 1),
	(24, 'sidebar2_content', 'sidebar2_content', '[We meet on every Friday]\r\nat Atithi Resort & Spa\r\nLakeside, Pokhara, Nepal.\r\nFeb-Oct. (6:00pm to 7:00pm) \r\nNov-Jan. (5:30pm to 6:30pm).', 1),
	(25, 'footer_message', 'footer_message', 'Rotary Club of Fishtail Pokhara. All Rights Reserved. ', 1),
	(26, 'slider_category_id', 'slider_category_id', '45', 1),
	(27, 'telephone_1', 'telephone_1', '+977 98560-21476', 1),
	(28, 'telephone_2', 'telephone_2', 't2', 1),
	(29, 'mobile_1', 'mobile_1', 'm1', 1),
	(30, 'mobile_2', 'mobile_2', 'm2', 1),
	(31, 'email_1', 'email_1', 'info@rotaryfishtail.org', 1),
	(32, 'email_2', 'email_2', 'e2', 1),
	(33, 'location', 'location', 'Rotary Club of Pokhara Fishtail, Atithi Resort & Spa, Lakeside, Pokhara, Nepal', 1),
	(34, 'contact', 'contact', 'Submit', 1),
	(41, 'header_logo', 'header_logo', '5.png', 1),
	(42, 'sidebar1_picture', 'sidebar1_picture', 'rot-brazil.gif', 1),
	(43, 'sidebar2_picture', 'sidebar2_picture', '232828-1406991727.jpg', 1),
	(44, 'no_of_message_on_home_page', 'no_of_message_on_home_page', '3', 1),
	(46, 'no_of_projects_on_home_page', 'no_of_projects_on_home_page', '30', 1),
	(49, 'message_category_id', 'message_category_id', '39', 1),
	(61, 'project_category_id', 'project_category_id', '40', 1),
	(62, 'no_of_news_on_home_page', 'no_of_news_on_home_page', '5', 1),
	(67, 'news_category_id', 'news_category_id', '41', 1),
	(68, 'welcome_message', 'welcome_message', 'TO ROTARY DIST-3292 OFFICIAL WEBSITE', 1),
	(69, 'introduction', 'introduction', 'Rotary International is a voluntary organization of business and professional leaders which provide humanitarian servicesone to help to build goodwill and peace in the world. There are more than 1.2 million (12 Lakhs) members 34000 Rotary clubs in over 200 countries and geographical areas. [Rotary\'s top priority is the global graphical eradication of polio.] Founded in Chicago in 1905, The Rotary foundation has awarded more than U.S. $ 2.1 billion in grants, which are administered at the local level by Rotary Clubs.', 1),
	(70, 'introduction_article_id', 'introduction_article_id', '53', 1);
/*!40000 ALTER TABLE `tbl_settings` ENABLE KEYS */;


-- Dumping structure for table db_rotaryfishtail_new.tbl_users
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `facebook_id` bigint(20) DEFAULT NULL,
  `username` varchar(111) NOT NULL,
  `group_id` int(22) unsigned NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `pass` varchar(111) NOT NULL,
  `address_unit` varchar(111) NOT NULL,
  `address_street` varchar(111) NOT NULL,
  `address_suburb` varchar(111) NOT NULL,
  `address_state_id` int(11) NOT NULL,
  `contact_emails` varchar(111) NOT NULL,
  `status` int(11) NOT NULL,
  `verification_code` varchar(111) NOT NULL,
  `last_login_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `facebook_id` (`facebook_id`,`username`),
  KEY `FK_tbl_users_tbl_groups` (`group_id`),
  CONSTRAINT `FK_tbl_users_tbl_groups` FOREIGN KEY (`group_id`) REFERENCES `tbl_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=latin1;

-- Dumping data for table db_rotaryfishtail_new.tbl_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` (`id`, `facebook_id`, `username`, `group_id`, `first_name`, `last_name`, `email`, `pass`, `address_unit`, `address_street`, `address_suburb`, `address_state_id`, `contact_emails`, `status`, `verification_code`, `last_login_date`, `created_at`, `updated_at`) VALUES
	(25, NULL, 'superadmin', 1, 'ram', 'bahadur', 'raxizel@gmai.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'pokhara', 'kajipokhri', 'suburb', 0, '', 1, '', '2015-05-09', '2015-05-09 00:00:00', '2015-05-09 00:00:00'),
	(181, NULL, 'admin', 1, 'ramesh', 'bahadur', 'raxizel@gmai.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'pokhara', 'kajipokhri', 'suburb', 0, '', 1, '', '2015-05-09', '2015-05-09 00:00:00', '2015-05-09 00:00:00');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
