$(function(){

	$('ul.pagination .ajax a').on("click", function (e) {
		$('.campaign-donation-box').html("Loading ...");
		e.preventDefault();
		var href=$(this).attr('href');					
		console.log("href:"+href);
		var href_parts = href.split("/");		
		// console.log("href_parts:"+href_parts);
		var count = href_parts.length;		
		// console.log("count:"+count);
		var offset=href_parts[count-1];
		console.log("offset:"+offset);
		var url=href+"/"+offset;
		load_doantions(url);
	});

	function load_doantions(url){
		console.log(url);
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'html',
			success: function(data) {
				$('.campaign-donation-box').html(data);
			},
			error: function(xhr,status,error){
				console.log("Error: " + xhr.status + ": " + xhr.statusText);
			}
		});
	}
});