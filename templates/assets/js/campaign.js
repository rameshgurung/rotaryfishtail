$(function(){

	$('input[type="file"]').on('change',function(){
		var file = $(this)[0].files[0];
		var maxSize = 2097152;
		var allowedType = ["image/jpeg","image/jpg","image/gif",
		"image/png"];
		var ret = checkFileUpload(file, maxSize, allowedType);
		if(!ret){
			$(this).val('');
		}
	});

	function checkFileUpload(file, maxSize, allowedType)
	{
		//check allowed file type
		var validFile = false;
		for (i = 0; i < allowedType.length; i++) {
			// console.log(file.type);
			if(file.type == allowedType[i])
			{
				validFile = true;
			}
		}
		if(!validFile){
			alert("Invalid file type. Please select another file.");
			return false;
		}
		//check maxfile size
		if(file.size >= maxSize)
		{
			maxSize = maxSize/1024/1024;
			alert("Maximum file size allowed is "+ maxSize + " MB.");
			return false;
		}
		return true;		
	}

});