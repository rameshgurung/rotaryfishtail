</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->
<!--MAIN NAVIGATION-->
<!--===================================================-->
<nav id="mainnav-container">
  <div id="mainnav">


    <!--Menu-->
    <!--================================-->
    <div id="mainnav-menu-wrap">
      <div class="nano">
        <div class="nano-content">
          <ul id="mainnav-menu" class="list-group">

            <li class="list-header">Navigation</li>



            <?php if(permission_permit(['administrator-donation'])) {?>            
            <li>
              <a href="<?=base_url('donation/admin')?>">
                <i class="fa fa-usd"></i>
                <span class="menu-title">
                  <strong>Donation</strong>
                  <span class="pull-right badge badge-success">
                    <?php 
                    $param['module']='donation'; 
                    echo show_total($param);
                    ?>
                  </span>
                </span>
              </a>
            </li>
            <?php } ?>

            <li class="list-divider"></li>

            <!--Category name-->
            <li class="list-header">Manage</li>


            <?php if(permission_permit(['administrator-fund-category'])) {?>
            <!-- fund-category -->
            <li>

              <a href="#">
                <i class="fa fa-navicon"></i>
                <span class="menu-title">Fund Categories
                  <span class="pull-right badge badge-info">
                    <?php 
                    $param['module']='fund_category'; 
                    echo show_total($param);
                    ?>
                  </span>
                </span>
                <i class="arrow"></i>
              </a>

              <ul class="collapse">
                <li><a href="<?=base_url('fund_category')?>">List</a></li>
                <?php if(permission_permit(['add-fund-category'])) {?>
                <li> <a href="<?=base_url()?>fund_category/add">Add</a></li>
                <?php } ?>                
              </ul>

            </li>
            <!-- fund-category -->
            <?php } ?>

            <?php if(permission_permit(['administrator-campaign'])) {?>
            <!-- campaigns -->
            <li>
              <a href="#">
                <i class="fa fa-th"></i>
                <span class="menu-title">Campaigns
                  <span class="pull-right badge badge-info">
                    <?php 
                    $param['module']='campaign'; 
                    echo show_total($param);
                    ?>
                  </span>
                </span>
                <i class="arrow"></i>
              </a>
              <ul class="collapse">
                <li><a href="<?=base_url('campaign/admin')?>">List</a></li>
                <?php if(permission_permit(['add-campaign'])) {?>
                <li> <a href="<?=base_url('campaign/admin/add')?>">Add</a></li>
                <?php } ?>
                <!-- <li> <a href="<?=base_url('report/fund_category/most_campaigns')?>">Most</a></li>                       -->
              </ul>
            </li>
            <!-- campaigns -->
            <?php } ?>

            <?php if(permission_permit(['administrator-donee'])) {?>
            <!-- fund-donee -->
            <li>
              <a href="javascritp:;">
                <i class="fa fa-users"></i>
                <span class="menu-title">Donee
                  <span class="pull-right badge badge-info">
                    <?php 
                    $param['module']='donee'; 
                    echo show_total($param);
                    ?>
                  </span>
                </span>
                <i class="arrow"></i>
              </a>
              <ul class="collapse">
                <li><a href="<?=base_url('user/donee')?>">List</a></li>
                <?php if(permission_permit(['add-donee'])) {?>
                <li> <a href="<?=base_url('user/donee/add')?>"/>Add</a></li>
                <?php } ?>
              </ul>
            </li>
            <!-- fund-donee -->
            <?php } ?>

            <?php             
            $user=permission_permit(['administrator-user']);
            $group=permission_permit(['administrator-group']);
            $permission=permission_permit(['administrator-permission']);

            if($user or $group or $permission) 
            {
              ?>
              <!-- users, groups and permission -->
              <li>
                <a href="<?php echo base_url('user')?>">
                  <i class="fa fa-briefcase"></i>
                  <span class="menu-title">
                    <?php
                    if($user and $group)
                      echo 'Users & Groups';
                    elseif ($user)
                      echo 'Users';
                    elseif ($group)
                      echo 'Groups';
                    ?>
                  </span>
                  <i class="arrow"></i>
                </a>
                <ul class="collapse">
                  <?php if(permission_permit(['administrator-group'])) {?>
                  <li>
                    <a href="<?=base_url('group')?>">Groups
                      <span class="pull-right badge badge-info">
                       <?php 
                       $param['module']='group'; 
                       echo show_total($param);
                       ?>
                     </span>
                   </a>
                 </li>
                 <?php } ?>
                 <?php if(permission_permit(['administrator-user'])){?>            
                 <li>
                  <a href="<?=base_url('user')?>">Users
                    <span class="pull-right badge badge-info">
                      <?php 
                      $param['module']='user'; 
                      echo show_total($param);
                      ?>
                    </span>
                  </a>
                </li>
                <?php } ?>
                <?php if(permission_permit(['administrator-permission'])){?>            
                <li>
                  <a href="<?=base_url('group/permission')?>">Permissions
                    <span class="pull-right badge badge-info">
                      <?php 
                      $param['module']='permission'; 
                      echo show_total($param);
                      ?>
                    </span></a>
                  </li>
                  <?php } ?>
                </ul>
              </li>
              <!-- users, groups and permission -->
              <?php 
            } 
            ?>


            <?php if(permission_permit(['administrator-setting'])){?>            
            <!-- setting -->           
            <li>
              <a href="<?=base_url('setting')?>">
                <i class="fa fa-wrench"></i>
                <span class="menu-title">
                  <strong>Setting</strong>
                  <span class="pull-right badge badge-info">
                    <?php 
                    $param['module']='setting'; 
                    echo show_total($param);
                    ?>
                  </span>
                </span>
              </a>
            </li>
            <!-- setting -->
            <?php } ?>


            <!--Category name-->
            <li class="list-header">Content</li>

            <!-- header-content -->
            <li>
              <a href="#">
                <i class="fa fa-list-alt"></i>
                <span class="menu-title">Header Section</span>
                <i class="arrow"></i>
              </a>
              <ul class="collapse">
                <li> <a href="<?php echo base_url("article/edit/".ARTICLE_WORK)?>">How it works</a></li>
                <!-- <li> <a href="#">Success Stories</a></li> -->
                <li> <a href="<?php echo base_url("article/category/".CATEGORY_HELP)?>">Help</a></li>
              </ul>
            </li>
            <!-- header-content -->

            <!-- footer-content -->
            <li>
              <a href="#">
                <i class="fa fa-table"></i>
                <span class="menu-title">Footer Section</span>
                <i class="arrow"></i>
              </a>
              <ul class="collapse">
                <li> <a href="<?=base_url("article/edit/".ARTICLE_ABOUT_US)?>">About Us</a></li>
                <li> <a href="<?=base_url("article/category/".CATEGORY_HELP)?>">How we Help</a></li>
                <li> <a href="<?=base_url("article/edit/".ARTICLE_LINKS)?>">Useful Links</a></li>
              </ul>
            </li>
            <!-- footer-content -->


          </ul>


        </div>
      </div>
    </div>
    <!--================================-->
    <!--End menu-->

  </div>
</nav>
<!--===================================================-->
<!--END MAIN NAVIGATION-->


</div>



<!-- FOOTER -->
<!--===================================================-->
<footer id="footer">

  <!-- Visible when footer positions are fixed -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <div class="pull-right">
    <ul class="footer-list list-inline">
      <!-- <li>
        <p class="text-sm">SEO Proggres</p>
        <div class="progress progress-sm progress-light-base">
          <div style="width: 80%" class="progress-bar progress-bar-danger"></div>
        </div>
      </li>

      <li>
        <p class="text-sm">Online Tutorial</p>
        <div class="progress progress-sm progress-light-base">
          <div style="width: 80%" class="progress-bar progress-bar-primary"></div>
        </div>
      </li>
      <li>
        <button class="btn btn-sm btn-dark btn-active-success">Checkout</button>
      </li> -->
    </ul>
  </div>



  <!-- Visible when footer positions are static -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <div class="hide-fixed pull-right pad-rgt">&#0169; 2015 <?=config_item('developer')?></div>



  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

  <!-- <p class="pad-lft">&#0169; 2015 <?=config_item('developer')?></p> -->



</footer>
<!--===================================================-->
<!-- END FOOTER -->


<!-- SCROLL TOP BUTTON -->
<!--===================================================-->
<button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
<!--===================================================-->



</div>
<!--===================================================-->
<!-- END OF CONTAINER -->


<!--JAVASCRIPT-->
<!--=================================================-->



<!--BootstrapJS [ RECOMMENDED ]-->
<!-- <script src="<?=admin_template_asset_path()?>/js/bootstrap.min.js"></script> -->


<!--Fast Click [ OPTIONAL ]-->
<script src="<?=admin_template_asset_path()?>/plugins/fast-click/fastclick.min.js"></script>


<!--Nifty Admin [ RECOMMENDED ]-->
<script src="<?=admin_template_asset_path()?>/js/nifty.min.js"></script>


<!--Morris.js [ OPTIONAL ]-->
<!-- <script src="<?=admin_template_asset_path()?>/plugins/morris-js/morris.min.js"></script> -->
<script src="<?=admin_template_asset_path()?>/plugins/morris-js/raphael-js/raphael.min.js"></script>


<!--Sparkline [ OPTIONAL ]-->
<script src="<?=admin_template_asset_path()?>/plugins/sparkline/jquery.sparkline.min.js"></script>


<!--Skycons [ OPTIONAL ]-->
<script src="<?=admin_template_asset_path()?>/plugins/skycons/skycons.min.js"></script>


<!--Switchery [ OPTIONAL ]-->
<script src="<?=admin_template_asset_path()?>/plugins/switchery/switchery.min.js"></script>


<!--Bootstrap Select [ OPTIONAL ]-->
<script src="<?=admin_template_asset_path()?>/plugins/bootstrap-select/bootstrap-select.min.js"></script>

<!-- template validation -->
<!--Bootstrap Validator [ OPTIONAL ]-->
<!-- <script src="<?=admin_template_asset_path()?>/plugins/bootstrap-validator/bootstrapValidator.min.js"></script> -->
<!--Masked Input [ OPTIONAL ]-->
<!-- <script src="<?=admin_template_asset_path()?>/plugins/masked-input/jquery.maskedinput.min.js"></script> -->
<!-- template validation -->

<!--Demo script [ DEMONSTRATION ]-->
<!-- <script src="<?=admin_template_asset_path()?>/js/demo/nifty-demo.min.js"></script> -->


<!--Specify page [ SAMPLE ]-->
<!-- <script src="<?=admin_template_asset_path()?>/js/demo/dashboard.js"></script> -->
<!--Specify page [ SAMPLE ]-->
<script src="<?=admin_template_asset_path()?>/js/custom.js"></script>



<!-- footables -->
<!-- <link href="<?=admin_template_asset_path()?>/plugins/fooTable/css/footable.core.css" rel="stylesheet">
<script src="<?=admin_template_asset_path()?>/plugins/fooTable/dist/footable.all.min.js"></script>
<script src="<?=admin_template_asset_path()?>/js/demo/tables-footable.js"></script>
--><!-- footables -->


<!-- nestable -->
<!-- <script src="<?=base_url()?>templates/assets/nestable/jquery-ui-1.9.1.custom.min.js"></script>
<script src="<?=base_url()?>templates/assets/nestable/jquery.mjs.nestedSortable.js"></script>
--><!-- nestable -->


<!-- core jquery -->
<link rel="stylesheet" href="<?=base_url()?>/templates/assets/jquery-v1.11.4/jquery-ui.css">
<script src="<?=base_url()?>/templates/assets/jquery-v1.11.4/jquery-ui.js"></script>
<!-- core jquery -->


<!-- bootstrap tinymice v 3 -->
<script src="<?=base_url()?>/templates/assets/bootstrap/v3/components/wysiwyg/wysihtml5x-toolbar.min.js"></script>
<script src="<?=base_url()?>/templates/assets/bootstrap/v3/core/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>/templates/assets/bootstrap/v3/components/wysiwyg/handlebars.runtime.min.js"></script>
<script src="<?=base_url()?>/templates/assets/bootstrap/v3/components/wysiwyg/bootstrap3-wysihtml5.min.js"></script>
<!-- bootstrap tinymice v 3 -->

<!-- bootsrtap components -->
<script src="<?=base_url()?>/templates/assets/bootstrap/components/bootbox-v4.4.0/bootbox.min.js"></script>
<!-- bootsrtap components -->


<!--Form validation [ SAMPLE ]-->
<!-- <script src="<?=admin_template_asset_path()?>/js/demo/form-validation.js"></script> -->
<script src="<?php echo template_path('assets/js/campaign.js')?>" type="text/javascript"></script>

<link rel="stylesheet" href="<?=base_url()?>/templates/assets/nestable/nestable.css">


<script>

  $(function(){
    try{

     $('a.delete').on("click", function(e) {
      e.preventDefault();
      var url = $(this).attr('href');
      // console.log(url);
      bootbox.confirm("Are you sure?", function(result) {
        if(result){
          window.location=url;
        }
      });
    });


     $('.textarea').wysihtml5();

     var validate='<?php echo $this->config->item("enabe_jq_validation_backend")?>';
     console.log(validate);
     if(validate==1){
      $("#validate_form").validate();

    }

    
    // tinymce.init({
    //   selector: "#mytextarea",
    //   fontsize_formats: "8pt 10pt 12pt 14pt 16pt 18pt 20pt 24pt 30pt 36pt 40pt",
    //   theme: "modern",
    //         // width: 300,
    //         height: 200,
    //         menubar: true,
    //         browser_spellcheck : true,
    //         codemirror: {
    //           path: '../../../codemirror',
    //           indentOnInit: true,
    //           extraKeys: {
    //             'Ctrl-Space': 'autocomplete'
    //           },
    //           config: {
    //             lineNumbers: true,
    //           }
    //         },
    //         image_advtab: true,
    //         // content_css: "css/tinymce.css",
    //         content_css: "<?php echo base_url('templates/assets/editor/css/tinymce.css')?>",
    //         plugins: [
    //         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
    //         "searchreplace wordcount visualblocks visualchars code codemirror fullscreen insertdatetime media nonbreaking",
    //         "save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
    //         ],
    //         toolbar: "fullscreen | undo redo | fullpage | styleselect fontselect fontsizeselect | forecolor backcolor emoticons | bold italic underline | strikethrough superscript subscript | fontsize | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | code preview print", 
    //         external_filemanager_path:"<?php echo base_url().'plugin/filemanager/'?>",
    //         // external_filemanager_path:"http://localhost/cel/2015/aug/prac/editor/filemanager/",
    //         filemanager_title:"Filemanager" ,
    //         // external_plugins: { "filemanager" : "http://localhost/cel/2015/aug/prac/editor/filemanager/plugin.min.js"},
    //         external_plugins: { "filemanager" : "<?php echo base_url().'templates/assets/editor/filemanager/plugin.min.js'?>" },

    //         style_formats: [
    //         {title: 'Heading 1', block: 'h1'},
    //         {title: 'Heading 2', block: 'h2'},
    //         {title: 'Heading 3', block: 'h3'},
    //         {title: 'Heading 4', block: 'h4'},
    //         {title: 'Heading 5', block: 'h5'},
    //         {title: 'Heading 6', block: 'h6'},
    //         {title: 'Blockquote', block: 'blockquote', styles: {color: '#333'}},
    //         {title: 'Pre Formatted', block: 'pre'},
    //         {title: 'code', block: 'pre', classes: 'code'},
    //         ],
    //         // relative_urls: false,
    //         // remove_script_host : true
    //       });


}
catch(e){
  console.log(e.message)
}

//hack pagination for repsonsive
$('<br class="clear">').insertAfter('ul.pagination');

});
</script>
<style>
  .clear{
    clear: both;
  }
</style>

<?php //show_pre(get_session('logged_in_user')); ?>

</body>

</html>
<?php if(ENVIRONMENT=='development'){
  // show_pre($this->session->userdata);
}
?>


