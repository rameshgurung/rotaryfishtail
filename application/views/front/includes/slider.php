<script type="text/javascript" src="<?php echo base_url('templates/front/scripts/jquery.cycle.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('templates/front/scripts/jquery.cycle.setup.js')?>"></script>

<div class="header">
  <div class="head">
    <div class="headright">
      <div id="container" class="clear">
        <div id="homepage" class="clear">
          <div class="fl_left">
            <div id="hpage_slider">

              <?php 
              $slider_show_total=get_setting_value('slider_show_total')?:3;
              
              $config['path']="uploads/tinyMCE/main/slider";
              $config['file_types']=array('jpg','png','gif','jpeg');
              
              $slider_response=get_folder_files($config);
              if($slider_response['success'] && is_array($slider_response['data']) && count($slider_response['data'])>0){  
                $slider_images=$slider_response['data'];
                for ($i=0; $i < $slider_show_total; $i++) { 
                  if($i==count($slider_images)) break; ?>
                  <?php
                  echo '<div class="item"><img src="'.base_url($config['path'].'/'.$slider_images[$i]).'" alt="" height="350" width="487" /></div>';
                  ?>
                  <?php 
                }
              }else{
                echo "Error, ".$slider_response['data'];
              }

              ?>  
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- welcome -->
    <div id="tagline">
      <h1 id="welcome">Welcome</h1>
      <h3>
        <?php echo get_setting_value('Welcome_message');?>
        <!-- To Rotary Dist-3292 Official Website -->
      </h3>
    </div>
    <!-- welcome -->

    <!-- introducion -->
    <div class="section">
      <p align="justify" >
        <h1 id="h1_intro">Introduction</h1>
        <span style="">
          <?php echo get_setting_value('introduction');?>
        </span>
        <p>
          <?php 
          $id=get_setting_value('introduction_article_id');
          $page=get_article(array('id'=>$id));
          if($page){ ?>          
          <a href="<?php echo base_url($page['slug']);?>" class="first">Learn More About Us</a>
          <?php }
          ?>
        </div>
      </div>
    </div>
    <!-- introducion -->
