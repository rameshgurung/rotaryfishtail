<div id="footer">
	<div id="img-table">
		<div>
			<img src="<?php echo base_url('templates/front/images/partners/1.png')?>" alt="" height="130" width="130" />
			<img src="<?php echo base_url('templates/front/images/partners/2.png')?>" alt="" height="130" width="130" />
			<img src="<?php echo base_url('templates/front/images/partners/3.png')?>" alt="" height="130" width="130" />
			<img src="<?php echo base_url('templates/front/images/partners/4.png')?>" alt="" height="130" width="130" />
			<img src="<?php echo base_url('templates/front/images/partners/5.png')?>" alt="" height="130" width="130" />
		</div>
	</div>
	<div class="visitor">
		<br><h3 style="float:left">Total Visitor</h3><img src="http://www.freecountercode.com//Hit_counters_4036546.gif" >
	</div>  		
</div>
<div class="crfooter">
	<div class="copyright">

		<p style="float:left;">
			© 2013-<?=date('Y')?> | 
			<?php echo get_setting_value('footer_message')?>
		</p>     
		<span><p>Website developed by <a href="http://www.celosiadesigns.com.np/" target="_blank">
			<img src="<?php echo base_url('templates/front/images/clogo.png')?>" width="200px" height="45px"/>
		</div>
		

	</div>


	<script type="text/javascript">
		$(function() {
			var $cn_next	= $('#cn_next');
			var $cn_prev	= $('#cn_prev');
			var $cn_list 	= $('#cn_list');
			var $pages		= $cn_list.find('.cn_page');
			var cnt_pages	= $pages.length;
			var page		= 1;
			var $items 		= $cn_list.find('.cn_item');
			var $cn_preview = $('#cn_preview');
			var current		= 1;

			$items.each(function(i){
				var $item = $(this);
				$item.data('idx',i+1);

				$item.bind('click',function(){
					var $this 		= $(this);
					$cn_list.find('.selected').removeClass('selected');
					$this.addClass('selected');
					var idx			= $(this).data('idx');
					var $current 	= $cn_preview.find('.cn_content:nth-child('+current+')');
					var $next		= $cn_preview.find('.cn_content:nth-child('+idx+')');

					if(idx > current){
						$current.stop().animate({'top':'-300px'},600,'easeOutBack',function(){
							$(this).css({'top':'310px'});
						});
						$next.css({'top':'310px'}).stop().animate({'top':'5px'},600,'easeOutBack');
					}
					else if(idx < current){
						$current.stop().animate({'top':'310px'},600,'easeOutBack',function(){
							$(this).css({'top':'310px'});
						});
						$next.css({'top':'-300px'}).stop().animate({'top':'5px'},600,'easeOutBack');
					}
					current = idx;
				});
			});

			$cn_next.bind('click',function(e){
				var $this = $(this);
				$cn_prev.removeClass('disabled');
				++page;
				if(page == cnt_pages)
					$this.addClass('disabled');
				if(page > cnt_pages){ 
					page = cnt_pages;
					return;
				}	
				$pages.hide();
				$cn_list.find('.cn_page:nth-child('+page+')').fadeIn();
				e.preventDefault();
			});
			$cn_prev.bind('click',function(e){
				var $this = $(this);
				$cn_next.removeClass('disabled');
				--page;
				if(page == 1)
					$this.addClass('disabled');
				if(page < 1){ 
					page = 1;
					return;
				}
				$pages.hide();
				$cn_list.find('.cn_page:nth-child('+page+')').fadeIn();
				e.preventDefault();
			});

		});
	</script>

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('div.navigation').css({'width' : '185px', 'float' : 'left'});
			$('div.content').css('display', 'block');

			var onMouseOutOpacity = 0.67;
			$('#thumbs ul.thumbs li').opacityrollover({
				mouseOutOpacity:   onMouseOutOpacity,
				mouseOverOpacity:  1.0,
				fadeSpeed:         'fast',
				exemptionSelector: '.selected'
			});

			var gallery = $('#thumbs').galleriffic({
				delay:                     2500,
				numThumbs:                 14,
				preloadAhead:              10,
				enableTopPager:            true,
				enableBottomPager:         true,
				maxPagesToShow:            7,
				imageContainerSel:         '#slideshow',
				controlsContainerSel:      '#controls',
				captionContainerSel:       '#caption',
				loadingContainerSel:       '#loading',
				renderSSControls:          true,
				renderNavControls:         true,
				playLinkText:              'Play Slideshow',
				pauseLinkText:             'Pause Slideshow',
				prevLinkText:              '&lsaquo; Previous Photo',
				nextLinkText:              'Next Photo &rsaquo;',
				nextPageLinkText:          '&rsaquo;',
				prevPageLinkText:          '&lsaquo;',
				enableHistory:             false,
				autoStart:                 false,
				syncTransitions:           true,
				defaultTransitionDuration: 900,
				onSlideChange:             function(prevIndex, nextIndex) {
					this.find('ul.thumbs').children()
					.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
					.eq(nextIndex).fadeTo('fast', 1.0);
				},
				onPageTransitionOut:       function(callback) {
					this.fadeTo('fast', 0.0, callback);
				},
				onPageTransitionIn:        function() {
					this.fadeTo('fast', 1.0);
				}
			});
});
</script>
</body>
</html>