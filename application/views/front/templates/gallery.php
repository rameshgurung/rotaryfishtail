    <!-- for gallery -->
    <link rel="stylesheet" href="<?php echo base_url('templates/front/gallery/css/gal_style.css')?>" />
    <script type="text/javascript" src="<?php echo base_url('templates/front/gallery/js/jquery-1.3.2.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('templates/front/gallery/js/jquery.galleriffic.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('templates/front/gallery/js/jquery.opacityrollover.js')?>"></script>
    <script type="text/javascript"><!--
      document.write('<style type="text/css">.noscript { display: none; }</style>');
    </script>
    <!-- for gallery -->



    <div id="body">
      <div id="content">
        <div id="<?php echo $menu['sidebar']=='Y'?'half_content':'full_content'?>">     
          <h2>Club Member's Information</h2>

          <?php
          if($gallery_response['success'] && is_array($gallery_response['data']) && count($gallery_response['data'])>0){  
            $images=$gallery_response['data']; 
            ?>
            <div class="page">
              <div id="gallery" class="content">
                <div id="controls" class="controls"></div>
                <div class="slideshow-container">
                  <div id="loading" class="loader"></div>
                  <div id="slideshow" class="slideshow"></div>
                </div>
                <div id="caption" class="caption-container"></div>
              </div>
              <div id="thumbs" class="navigation">
                <ul class="thumbs noscript">
                  <?php 
                  foreach ($images as $image) { ?>
                  <li>
                    <a class="thumb" href="<?php echo base_url($path.'/'.$image);?>">
                      <img src="<?php echo base_url($path.'/'.$image);?>" alt="" height="75" width="75" />
                    </a>

                    <div class="caption">
                      <div class="image-title"></div>
                      <div class="image-desc"></div>
                    </div>

                  </li>
                  <?php }
                  ?> 
                </ul>
              </div>
            </div>
            <?php
          }
          else{
            echo "Error, ".$gallery_response['data'];
          }
          ?>
        </div>
        <?php 
        if($menu['sidebar']=='Y') {
          $this->load->view('front/includes/sidebar.php');
        }
        ?>

      </div>
    </div>
