<div id="body">
	<div id="content">
		
		<div id="<?php echo $menu['sidebar']=='Y'?'half_content':'full_content'?>">			

			<span>
				<h2><?php echo isset($category['name'])?$category['name']:''?></h2>
				<br>

				<?php if(isset($articles) && count($articles)) { ?>

				<div id="accordion" style="" >
					<?php foreach ($articles as $key=>$article) { ?>

					<!--<div>-->
					<h3><a href="#" class="title"> <?php echo $article['name'] ?> </a></h3>
					<div>
						<?php  echo $article['content'] ?>
					</div>
					<!-- </div>-->					

					<?php } ?>
				</div>
				<?php } ?>

			</span>
		</div>

		<?php if($menu['sidebar']=='Y'){?>
		<?php $this->load->view('front/includes/sidebar.php') ?>
		<?php } ?>

	</div>
</div>

