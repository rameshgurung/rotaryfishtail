<div id="body">
	<div id="content">
		
		<div id="<?php echo $menu['sidebar']=='Y'?'half_content':'full_content'?>">			

			<span>
				<h2><?php echo isset($category['name'])?$category['name']:''?></h2>
				<br>

				<?php if(isset($articles) && count($articles)) { ?>
				<?php foreach ($articles as $key=>$article) { ?>

				<div class="memberbox" style="padding:10px;">
					<h3><?php echo $article['name']?></h3>
					<br>
					<span class="date">
						<?php echo format($article['created_at'])?>
					</span> 
					<a href="news.html" class="figure">
						<?php if($article['image'] && is_article_picture_exists($article['image'])){?>
						<img class="course" src="<?php echo base_url('uploads/files/pics/articles/news/'.$article['image'])?>" 
						width="200px" height="150px" style="float:left; margin-right:20px"/>
						<?php } else {?>
						<img class="course" src="<?php echo base_url('templates/assets/media/images/no_image_found.jpg')?>" 
						width="200px" height="140px"/>
						<?php } ?>
					</a>
					<p>
						<?php echo $article['content']?>
					</p>
				</div>	
				<br>					
				<br>					

				<?php } ?>
				<?php } ?>

			</span>
		</div>

		<?php if($menu['sidebar']=='Y'){?>
		<?php $this->load->view('front/includes/sidebar.php') ?>
		<?php } ?>

	</div>
</div>
