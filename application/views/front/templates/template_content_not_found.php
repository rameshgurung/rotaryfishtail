<div id="body">
	<div id="content">		
		<div id="full_content" class="error">			
			<span>				
				<h1><?php echo isset($message)?$message:'200, Content / Template not found'?></h1>
				<p>The Page you are looking for doesn't have content or template not found.</p>
				<a class="btn btn-success" href="<?php echo base_url('')?>">GO BACK TO THE HOMEPAGE</a>
			</span>
		</div>
	</div>
</div>

<style>

	#full_content{
		text-align: center;
	}
	#full_content h1{
		text-align: center !important; 
	}
	.error h1{ font-size: 50px;}
	.error p{
		color:red;
		text-align: center;
	}
</style>