<div id="body">
	<div id="content">
		
		<div id="<?php echo $menu['sidebar']=='Y'?'half_content':'full_content'?>">			
			<span>				
				
					<?php ?>
					<h1 id="title"><?php echo isset($category['name'])?$category['name']:''?></h1>
					<?php  ?>
					<h2><?php echo isset($category['content'])?$category['content']:''?></h2>
					<br>	
					<?php if(isset($articles) && count($articles)) { ?>
					<?php foreach ($articles as $key=>$article) { ?>

					<div>
						<h3><?php echo $article['name']?></h3>
						<?php if($article['image'] && is_article_picture_exists($article['image'])){?>
						<?php 
						switch ($category['slug']) {
							case article_m::WIDGET_MESSAGE:{
								$path=article_m::WIDGET_MESSAGE;
								break;
							}
							case article_m::WIDGET_PROJECT:{
								$path=article_m::WIDGET_PROJECT;
								break;
							}
							case article_m::WIDGET_NEWS:{
								$path=article_m::WIDGET_NEWS;
								break;
							}
							default:{
								$path='/';
								break;
							}
						}
						?>						
						<img class="course" src="<?php echo is_article_picture_exists($path.'/'.$article['image'])?>" 
						width="200px" height="140px"/>
						<?php } else {?>
						<img class="course" src="<?php echo base_url('templates/assets/media/images/no_image_found.jpg')?>" 
						width="200px" height="140px"/>
						<?php } ?>
						<p>
							<?php echo $article['content']?>
						</p>
					</div>
					<?php } ?>
					<?php } ?>
				</span>
			</div>

			<?php if($menu['sidebar']=='Y'){?>
			<?php $this->load->view('front/includes/sidebar.php') ?>
			<?php } ?>

		</div>
	</div>
