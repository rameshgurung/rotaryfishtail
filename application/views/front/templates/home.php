</div>
<div id="body">
    <?php $this->load->view('front/includes/slider.php') ?>    


    <?php
    $widgets=default_categories();

    foreach ($widgets as $widget) {
        if($widget['slug']==article_m::WIDGET_MESSAGE){
            $message_widget=get_widget($widget['id']);
            $no_of_messages=get_setting_value('no_of_message_on_home_page')?:3;
        }
        elseif($widget['slug']==article_m::WIDGET_PROJECT){
            $project_widget=get_widget($widget['id']);
            $no_of_projects=get_setting_value('no_of_projects_on_home_page')?:3;
        }
        elseif($widget['slug']==article_m::WIDGET_NEWS){
            $news_widget=get_widget($widget['id']);
            $no_of_news=get_setting_value('no_of_news_on_home_page')?:3;
        }
    }
    ?>

    <!-- body -->
    <div id="body">
        <!-- getinvolved -->
        <div id="getinvolved">
            <!-- div un1 -->
            <div class="un1">
                <!-- cbox margin-top -->
                <div class="cbox margin-top">
                    <!-- tabs container-->
                    <div  style="float:left; width:630px">
                        <div id="tabs" style="overflow:hidden;">

                            <!-- tab heading -->
                            <ul>
                                <?php 
                                if($no_of_messages>0){  
                                    for ($i=0; $i < $no_of_messages; $i++) { 
                                        if($i==count($message_widget['articles'])) break; ?>
                                        <li><a href="#tabs-<?php echo $i+1?>"><?php echo $message_widget['articles'][$i]['title']?></a></li>
                                        <?php 
                                    }
                                }
                                ?>
                            </ul>
                            <!-- tab heading -->

                            <!-- tab body -->
                            <span class="tab_body">

                                <?php 
                                if($no_of_messages>0){  
                                    for ($i=0; $i < $no_of_messages; $i++) { 
                                        if($i==count($message_widget['articles'])) break; ?>
                                        <div id="tabs-<?php echo $i+1?>" style="height:665px; overflow:hidden">
                                            <h3 style="font-size:20px;"><?php echo $message_widget['articles'][$i]['name']?></h3><br>
                                            <div style="float:left;">
                                                <div class="photo">
                                                    <?php if($message_widget['articles'][$i]['image']) { ?>
                                                    <?php $image=is_picture_exists(article_m::file_path.article_m::WIDGET_MESSAGE.'/'.$message_widget['articles'][$i]['image']); ?>  
                                                    <img alt="" class="img-md" src="<?php echo $image;?>" width="120px" height="150">
                                                    <?php } else{ ?>
                                                    <img class="course" src="<?php echo base_url('templates/assets/media/images/no_image_found.jpg')?>"
                                                    alt="No Image" width="120px" height="150"> 
                                                    <?php } ?>
                                                </div>
                                                <div style="width:220px; clear:both; text-align:center; float:left; margin-left:-40px; height:70px">
                                                    <h3><?php echo $message_widget['articles'][$i]['title']?></h3>
                                                    <h5>
                                                        <?php echo $message_widget['articles'][$i]['image_title']?>
                                                        <br>
                                                        <?php echo $message_widget['articles'][$i]['image_title_2']?>
                                                    </h5>
                                                </div>
                                            </div>
                                            <div>
                                                <?php echo $message_widget['articles'][$i]['content']?>
                                            </div>
                                        </div>
                                        <?php 
                                    }
                                }
                                ?>
                            </span>
                            <!-- tab body -->
                        </div>
                    </div>
                    <!-- tabs container-->
                    <!-- sidebar -->
                    <?php $this->load->view('front/includes/sidebar.php') ?>
                    <!-- sidebar -->                    
                </div>
            </div>
            <!-- tabs container-->
        </div>
        <!-- cbox margin-top -->

        <!-- project & news -->
        <div class="body">
            <div class="nbody">

                <!-- project -->
                <div>
                    <div id="accordion" style="width:420px; float:left;">
                        <?php 
                        if($no_of_projects>0){  
                            for ($i=0; $i < $no_of_projects; $i++) { 
                                if($i==count($project_widget['articles'])) break; ?>
                                <h3><a href="#"><?php echo $project_widget['articles'][$i]['name']?></a></h3>
                                <div><?php echo $project_widget['articles'][$i]['content']?></div>
                                <?php 
                            }
                        }
                        ?>
                    </div>
                </div>
                <!-- project -->

                <!-- news -->
                <div class="figure">
                    <div class="cn_wrapper">

                        <!-- right news -->
                        <div id="cn_preview" class="cn_preview">
                            <?php
                            if($no_of_news>0){ 
                                for ($i=0; $i < $no_of_news; $i++) { ?>
                                <?php if($i==count($news_widget['articles'])) {echo "$i == ".count($news_widget['articles']); break;} ?>
                                
                                <div class="cn_content" style="top:1px;">
                                    <?php if($news_widget['articles'][$i]['image']) { ?>
                                    <?php $image=is_picture_exists(article_m::file_path.article_m::WIDGET_NEWS.'/'.$news_widget['articles'][$i]['image']); ?>  
                                    <img src="<?php echo $image;?>" width="219px" height="119px">
                                    <?php } else{ ?>
                                    <img src="images/news/majthum1.jpg" alt="" width="219px" height="119px">
                                    <?php } ?>
                                    <span class="cn_date"><?php echo $news_widget['articles'][$i]['created_at']?></span>
                                    <span>
                                        <br>
                                        <br>
                                        <?php 
                                        $content=$news_widget['articles'][$i]['content'];
                                        echo word_limiter(convert_accented_characters($content), 50)
                                        ?>
                                    </span>
                                    <a href="<?php echo base_url('news')?>" target="_blank" class="cn_more">Read more...</a>
                                </div>


                                <?php }
                            }
                            ?>
                        </div>

                        <div id="cn_list" class="cn_list">

                            <div class="cn_page" style="display:block;">
                                <?php
                                if($no_of_news>0){ 
                                    for ($i=0; $i < $no_of_news; $i++) { ?>
                                    <?php if($i==count($news_widget['articles'])) break; ?>
                                    <div class="cn_item selected">
                                        <h2><?php echo $news_widget['articles'][$i]['name']?></h2>
                                    </div>
                                    <?php }
                                }
                                ?>
                            </div>

                            <div class="cn_page">
                            </div>
                            <div class="cn_page">
                            </div>
                            <div class="cn_nav">
                                <a id="cn_prev" class="cn_prev disabled"></a>
                                <a id="cn_next" class="cn_next"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- news -->   

            </div>
        </div>
        <!-- project & news -->

    </div>
</div>


<style>
    .tab_body #tabs-1{
        padding: 17px !important;
    }
    
</style>
